// CSC3003S - Capstone Project - CHALK
// Alex Hall & Matthew Welham
// HLLALE006 & WLHMAT002

package tree;

import java.awt.event.MouseEvent;

public class TopHandle extends Handle {

	TopHandle(Node node) {
		super(node);
	}

	@Override
	public int getY() {
		return node.y - node.radius;
	}

	@Override
	public int getX() {
		return node.x;
	}

	@Override
	boolean connectToHandle(Handle otherHandle, Node otherNode, MouseEvent e) {
		if (otherHandle instanceof ChildHandle) {
			
			// Check for cycle
			if (node.isAncestorOf(otherNode)) {
				showCycleError(e);
				return true;
			}
			
			if (node.parent == null) {

				// No parent, so check if this is the root
				if (node.isRoot()) {
					Node.reassignRoot(otherNode);
				}
			} else {
				
				// Remove old parents
				
				// If this node is a left child
				if (node.parent.left == node) {
					node.parent.setLeft(null);
					
				} else {
					node.parent.setRight(null);
				}
			}
			
			ChildHandle childHandle = (ChildHandle) otherHandle;
			if (childHandle.getChild() != null) {
				childHandle.setChild(null);
			}
			childHandle.setChild(node);
			
			return true;
		}
		return false;
	}
	
	@Override
	void createNode() {
	}
}