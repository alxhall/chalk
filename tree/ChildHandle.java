// CSC3003S - Capstone Project - CHALK
// Alex Hall & Matthew Welham
// HLLALE006 & WLHMAT002

package tree;

import java.awt.event.MouseEvent;

/**
 * Abstract class representing one of the two lower Handles which represent the
 * connection points between Nodes and their children.
 */
abstract class ChildHandle extends Handle {

	/** Used to calculate position. Cached for efficiency */
	static final double SQRT_2_INV = 1 / Math.sqrt(2);

	ChildHandle(Node node) {
		super(node);
	}

	/**
	 * Return the y-coordinate of the ChildHandle's centre. Overrides Handle's
	 * method.
	 */
	@Override
	public int getY() {
		return (int) Math.round(node.y + node.radius * SQRT_2_INV);
	}

	/**
	 * Returns whether a two Nodes were successfully connected. This relies on
	 * the ChildHandle being connected to a TopHandle of another node without a
	 * cycle being formed. If an edge can be created between Nodes, then old
	 * edges may have to be deleted.
	 */
	@Override
	boolean connectToHandle(Handle otherHandle, Node otherNode, MouseEvent e) {

		// can only connect a ChildHandle to a TopHandle
		if (otherHandle instanceof TopHandle) {

			// check for a cycle and show error if one exists, also returning
			// from the method.
			if (otherNode.isAncestorOf(node)) {
				showCycleError(e);
				return true;
			}

			// no cycle, so perform the edge assignment

			// reassign the owning Node of this ChildHandle to be the new root
			// if we are assigning the old root to be the child of this Node.
			if (otherNode.isRoot()) {
				Node.reassignRoot(node);
			}

			// if the new child of the owning Node of this ChildHandle has a
			// parent, set this parent's relevant child reference to null.
			if (otherNode.parent != null) {
				if (otherNode.parent.left == otherNode) {
					otherNode.parent.setLeft(null);
				} else {
					otherNode.parent.setRight(null);
				}
			}

			// if the owning Node of this ChildHandle already has a relevant
			// child, set this child's parent to null.
			if (getChild() != null) {
				setChild(null);
			}

			setChild(otherNode);

			return true;
		}
		return false;
	}

	/**
	 * Creates a new subtree of Node(s) that are assigned to be the child(ren)
	 * of the owning Node of this ChildHandle.
	 */
	@Override
	void createNode() {
		if (getChild() == null) {
			// create a Node/subtree according to the amount of Nodes specified
			// by the JSpinner in the MainFrame
			Node newNode = node.createSubtree(node.canvas.getGUI()
					.getNumberOfNodesToCreate(), newEdgeEndX, newEdgeEndY
					+ Node.DEFAULT_RADIUS);

			setChild(newNode);

			// layout the subtree if applicable
			if (!(node.canvas.shouldLayoutTree())) {
				node.canvas.layoutSubtree(newNode, newEdgeEndX, newEdgeEndY
						+ Node.DEFAULT_RADIUS);
			}

			// update the Canvas of the owning Node
			node.canvas.updateTree();
		}
	}

	/**
	 * Method to be inherited to define how references to child Nodes are
	 * returned.
	 */
	abstract Node getChild();

	/** Method to be inherited to define how child Nodes are set. */
	abstract void setChild(Node otherNode);

}