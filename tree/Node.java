// CSC3003S - Capstone Project - CHALK
// Alex Hall & Matthew Welham
// HLLALE006 & WLHMAT002

package tree;

import gui.Canvas;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import animation.AnimationAction;
import animation.AnimationStep;
import animation.InstantAnimationAction;
import animation.Interpolator;

/**
 * The central class to the application. A Node intended to be used in a binary
 * tree.
 */
public class Node implements Serializable {

	private static final long serialVersionUID = -3808762085860912811L;

	static final int DEFAULT_RADIUS = 20;
	int radius = DEFAULT_RADIUS;

	private static final float MIN_FONT_SIZE = 10f;
	private float MAX_FONT_SIZE = 20f;

	private static final Color DEFAULT_FILL_COLOR = Color.white;
	private static final Color ROOT_FILL_COLOR = Color.cyan;

	private static final Color DEFAULT_OUTLINE_COLOR = Color.blue;
	private static final Color UNBALANCED_OUTLINE_COLOR = Color.red;

	private static final Color EDGE_COLOR = Color.black;

	public static final double DELETE_RADIUS_SQUARED = 50;

	int x;
	int y;

	transient Canvas canvas;

	Node left;
	Node right;
	Node parent;

	private transient Handle leftHandle;
	private transient Handle rightHandle;
	private transient Handle topHandle;
	private transient Handle[] handles;

	private boolean dragging;

	private boolean showHandles = false;

	/**
	 * The amount that the mouse was offset from the center, used to keep
	 * dragging consistent.
	 */
	private int dragOffsetX;
	private int dragOffsetY;

	private Handle activeHandle;

	private static final int NO_CONTENTS = Integer.MIN_VALUE;

	// animation constants
	private static final int LAYOUT_TIME = 300;
	private static final int CHANGE_COLOR_TIME = 500;
	private static final int MOVE_EDGE_TIME = 1000;
	private static final int MOVE_CONTENTS_TIME = 1500;

	private int contents = NO_CONTENTS;

	/** See getContour() */
	private List<int[]> contour;

	/**
	 * Horizontal 'distance' in a grid layout between the node and its children
	 * when in layout mode. See getContour().
	 */
	private int childDistance = -1;

	static Handle hoveringHandle = null;

	/** Purely for Serialisation purposes (saving of tree). */
	private boolean isSerializedRoot = false;

	private Color fillColor = DEFAULT_FILL_COLOR;

	private boolean inSortedPosition = true;

	private boolean movingLeftEdge;
	private boolean movingRightEdge;

	private int movingEdgeX;
	private int movingEdgeY;

	private boolean movingContents = false;

	private int movingContentsX;
	private int movingContentsY;

	public Node(int x, int y, Canvas canvas) {
		this.x = x;
		this.y = y;
		this.canvas = canvas;
		createHandles();
	}

	public Node(int x, int y, Canvas canvas, int contents) {
		this(x, y, canvas);
		this.contents = contents;
	}

	/**
	 * 
	 * @return : the x-coordinate of the Node's centre
	 */
	public int getX() {
		return x;
	}

	/**
	 * 
	 * @return : the y-coordinate of the Node's centre
	 */
	public int getY() {
		return y;
	}

	/**
	 * 
	 * @return : whether the Node is the serialized root
	 */
	public boolean getIsSerializedRoot() {
		return isSerializedRoot;
	}

	public void setIsSerializedRoot(boolean isRoot) {
		this.isSerializedRoot = isRoot;
	}

	public void setCanvas(Canvas canvas) {
		this.canvas = canvas;
	}

	/** Create the Handles of the Node. Generally called after loading a tree. */
	public void createHandles() {
		leftHandle = new LeftHandle(this);
		rightHandle = new RightHandle(this);
		topHandle = new TopHandle(this);
		handles = new Handle[] { leftHandle, rightHandle, topHandle };
	}

	/**
	 * 
	 * @return : the left child Node of the Node
	 */
	public Node getLeft() {
		return left;
	}

	/** Set the left child Node of the Node. */
	public void setLeft(Node newLeft) {
		if (newLeft == null) {
			if (this.left != null) {
				this.left.parent = null;
			}
			this.left = null;
		} else {
			this.left = newLeft;
			newLeft.parent = this;
		}
	}

	/**
	 * 
	 * @return : the right child Node of the Node.
	 */
	public Node getRight() {
		return right;
	}

	/** Set the right child Node of the Node. */
	public void setRight(Node newRight) {
		if (newRight == null) {
			if (this.right != null) {
				this.right.parent = null;
			}
			this.right = null;
		} else {
			this.right = newRight;
			newRight.parent = this;
		}
	}

	/**
	 * 
	 * @return : a Node array of the Node's children.
	 */
	public Node[] children() {
		return new Node[] { left, right };
	}

	/**
	 * 
	 * @return : a reference to the Node's parent Node.
	 */
	public Node getParent() {
		return parent;
	}

	/** Set the parent Node of the Node. */
	public void setParent(Node parent) {
		this.parent = parent;
	}

	/** Set the Node to be the root of the tree. */
	public void setToRoot() {
		canvas.setRoot(this);
	}

	/**
	 * 
	 * @return : whether or not this Node is the root of the tree.
	 */
	public boolean isRoot() {
		return canvas.getRoot() == this;
	}

	/**
	 * 
	 * @param e
	 *            : the MouseEvent being checked
	 * @return : whether or not the mouse pointer is within the bounds of the
	 *         Node.
	 */
	public boolean isClicked(MouseEvent e) {
		return isWithin(e.getX(), e.getY());
	}

	// a few methods follow which check whether coordinates lie within the Node
	// or a given position.

	public boolean isWithin(int x, int y) {
		return isWithinCircle(x, y, this.x, this.y, radius);
	}

	public static boolean isWithinCircle(MouseEvent e, int cx, int cy, int r) {
		return isWithinCircle(e.getX(), e.getY(), cx, cy, r);
	}

	public static boolean isWithinCircle(int x, int y, int cx, int cy, int r) {
		return (Math.sqrt(Math.pow(x - cx, 2) + Math.pow(y - cy, 2)) < r);
	}

	/** Move the Node and update the tree. */
	public void move(int x, int y) {
		this.x = x;
		this.y = y;
		canvas.changeTree();
	}

	/**
	 * Paint the Node to the Canvas
	 * 
	 * @param g
	 *            : the Graphics to update
	 */
	public void paintNode(Graphics g) {
		g.setColor(fillColor);

		fillCircle(g, x, y, radius);

		g.setColor(canvas.getGUI().isHighlightingUnbalancedNodes()
				&& Math.abs(calculateBalanceFactor()) > 1 ? UNBALANCED_OUTLINE_COLOR
				: DEFAULT_OUTLINE_COLOR);
		drawCircle(g, x, y, radius);

		if (showHandles) {
			for (Handle handle : handles) {
				handle.paint(g);
			}
		}
	}

	/** Animate the resetting of the Node's colour. */
	public void resetColor() {
		AnimationStep step = new AnimationStep(canvas);
		resetColor(step);
		step.start();
	}

	/** Set showHandles to false */
	public void hideHandles() {
		showHandles = false;
	}

	/** Animate the resetting of the Node's colour. */
	public void resetColor(AnimationStep step) {
		Color oldColor = fillColor;
		Color newColor = getUsualColor();
		if (!newColor.equals(oldColor)) {
			animateSetColor(newColor, step);
		}
	}

	/**
	 * 
	 * @return : the usual colour of the Node, dependent on whether or not it is
	 *         the root
	 */
	public Color getUsualColor() {
		return isRoot() ? ROOT_FILL_COLOR : DEFAULT_FILL_COLOR;
	}

	/**
	 * Paint the edges of the Node to the Canvas. Note that edges are drawn from
	 * parent to children.
	 * 
	 * @param g
	 *            : the Graphics to update
	 */
	public void paintEdges(Graphics g) {
		g.setColor(EDGE_COLOR);
		if (movingLeftEdge) {
			g.drawLine(leftHandle.getX(), leftHandle.getY(), movingEdgeX,
					movingEdgeY);
		} else if (left != null) {
			g.drawLine(leftHandle.getX(), leftHandle.getY(), left.topHandle
					.getX(), left.topHandle.getY());
		}
		if (movingRightEdge) {
			g.drawLine(rightHandle.getX(), rightHandle.getY(), movingEdgeX,
					movingEdgeY);
		} else if (right != null) {
			g.drawLine(rightHandle.getX(), rightHandle.getY(), right.topHandle
					.getX(), right.topHandle.getY());
		}
	}

	/**
	 * Paint the contents of the Node to the Canvas. The font size of the
	 * Contents is adjusted until the contents will fit inside the Node or until
	 * the font size has reached a minimum value.
	 * 
	 * @param g
	 *            : the Graphics to update
	 */
	public void paintContents(Graphics g) {
		if (hasContents() || canvas.getGUI().showBalanceFactors()) {

			float fontSize = MAX_FONT_SIZE + 1;
			String text = contents + "";
			g.setColor(Color.black);

			// Showing balance factors
			if (canvas.getGUI().showBalanceFactors()) {
				text = calculateBalanceFactor() + "";
				g.setFont(g.getFont().deriveFont(Font.ITALIC));
			} else if (canvas.getGUI().isSortedMode() && !inSortedPosition) {

				// Showing contents of unsorted nodes in red
				g.setColor(Color.red);
			}

			// Reduce the font size until the text fits
			int textX, textY;
			Rectangle2D bounds;
			do {
				fontSize--;
				g.setFont(g.getFont().deriveFont(fontSize));
				bounds = g.getFontMetrics().getStringBounds(text, g);

				textX = (int) Math.round(x - bounds.getWidth() / 2);
				textY = (int) Math.round((y - bounds.getHeight() / 2)
						+ g.getFontMetrics().getAscent());
			} while (!isWithin(textX, textY) && fontSize > MIN_FONT_SIZE);

			if (movingContents) {
				textX = (int) Math.round(movingContentsX - bounds.getWidth()
						/ 2);
				textY = (int) Math
						.round((movingContentsY - bounds.getHeight() / 2)
								+ g.getFontMetrics().getAscent());
			}

			g.drawString(text, textX, textY);
		}
	}

	static void fillCircle(Graphics g, int cx, int cy, int r) {
		g.fillOval(cx - r, cy - r, r * 2, r * 2);
	}

	/** Set the contents of the node to an integer entered by the user */
	private void requestNodeContents() {
		String newContents = (String) JOptionPane.showInputDialog(new JFrame(),
				"Enter the contents of the node:", "Edit node contents",
				JOptionPane.PLAIN_MESSAGE, null, null,
				contents == NO_CONTENTS ? "" : contents + "");

		// If a string was returned, say so.
		if (newContents != null) {
			try {
				contents = newContents.length() > 0 ? Integer
						.parseInt(newContents) : NO_CONTENTS;
			} catch (NumberFormatException ex) {
				canvas.getGUI().showErrorMessage(
						"Contents of node must be an integer value.",
						"Contents error");
				requestNodeContents();
			}
		}

		canvas.updateTree();
	}

	/**
	 * Handle a mouse click if possible for the Draw Tool
	 * 
	 * @param e
	 *            : the particular MouseEvent
	 * @return Whether or not the event is consumed (the click has been taken
	 *         care of)
	 */
	public boolean drawToolPressed(MouseEvent e) {

		// Double left click: set contents
		if (isClicked(e) && e.getClickCount() == 2
				&& e.getButton() == MouseEvent.BUTTON1) {
			requestNodeContents();
			return true;
		}

		// Check if a handle was clicked (creating a new node):
		for (Handle handle : handles) {
			boolean handled = handle.mousePressed(e);
			if (handled) {
				activeHandle = handle;
				return true;
			}
		}

		// Finally, if the rest of the node was clicked, initiate dragging
		// (moving)
		if (isClicked(e)) {
			startDragging(e);
			return true;
		}

		// If none of these happened, the mouse was probably nowhere near the
		// node. Report that the click event is unhandled.
		return false;
	}

	/**
	 * Handle a mouse click if possible for the edit contents tool
	 * 
	 * @param e
	 *            : the particular MouseEvent
	 * @return Whether or not the event is consumed (the click has been taken
	 *         care of)
	 */
	public boolean editContentsToolPressed(MouseEvent e) {
		if (isClicked(e)) {
			requestNodeContents();
			return true;
		}
		return false;
	}

	/**
	 * Handle a mouse click if possible for the move tool
	 * 
	 * @param e
	 *            : the particular MouseEvent
	 * @return Whether or not the event is consumed (the click has been taken
	 *         care of)
	 */
	public boolean moveToolPressed(MouseEvent e) {
		if (isClicked(e)) {
			startDragging(e);
			return true;
		}
		return false;
	}

	/** Initiate the moving of this node by a mouse drag */
	private void startDragging(MouseEvent e) {
		dragging = true;
		dragOffsetX = x - e.getX();
		dragOffsetY = y - e.getY();
	}

	/**
	 * Handle a mouse release for the draw tool
	 * 
	 * @param e
	 *            : the particular MouseEvent
	 */
	public void drawToolReleased(MouseEvent e) {
		for (Handle handle : handles) {
			handle.mouseReleased(e);
		}
		dragging = false;
		activeHandle = null;
	}

	/**
	 * Handle a mouse release for the move tool
	 * 
	 * @param e
	 *            : the particular MouseEvent
	 */
	public void moveToolReleased(MouseEvent e) {
		dragging = false;
	}

	/**
	 * Handle a mouse drag for the draw tool
	 * 
	 * @param e
	 *            : the particular MouseEvent
	 */
	public void drawToolDragged(MouseEvent e) {
		if (activeHandle != null) {
			activeHandle.mouseDragged(e);
		}

		// Refresh the appearance of all other nodes (to show handles to link a
		// new edge with)
		for (Node node : canvas.getNodes()) {
			if (node != this) {
				node.refreshAppearance(e);
			}
		}
		moveByDrag(e);
	}

	/**
	 * Handle a mouse release for the move tool
	 * 
	 * @param e
	 *            : the particular MouseEvent
	 */
	public void moveToolDragged(MouseEvent e) {
		moveByDrag(e);
	}

	/**
	 * Move the Node/subtree according to the MouseEvent.
	 * 
	 * @param e
	 *            : the particular MouseEvent
	 */
	private void moveByDrag(MouseEvent e) {
		if (dragging) {

			// Move the node without moving its subtree when the user holds down
			// shift while dragging
			if (canvas.isShiftKeyDown()) {
				move(e.getX() + dragOffsetX, e.getY() + dragOffsetY);
			} else {

				// Default: move the whole subtree at once
				moveSubtreeBy((e.getX() + dragOffsetX) - x,
						(e.getY() + dragOffsetY) - y);
			}
		}
	}

	/**
	 * Move the subtree of a Node by a given distance.
	 * 
	 * @param distanceToMoveX
	 *            : the horizontal distance to move the Nodes in the subtree
	 * @param distanceToMoveY
	 *            : the vertical distance to move the Nodes in the subtree
	 */
	private void moveSubtreeBy(int distanceToMoveX, int distanceToMoveY) {
		for (Node child : children()) {
			if (child != null) {
				child.moveSubtreeBy(distanceToMoveX, distanceToMoveY);
			}
		}
		moveBy(distanceToMoveX, distanceToMoveY);
	}

	/**
	 * Move the Node by the given distance
	 * 
	 * @param distanceToMoveX
	 *            : the horizontal distance to move the Nodes in the subtree
	 * @param distanceToMoveY
	 *            : the vertical distance to move the Nodes in the subtree
	 */
	private void moveBy(int distanceToMoveX, int distanceToMoveY) {
		move(x + distanceToMoveX, y + distanceToMoveY);
	}

	public static void drawCircle(Graphics g, int cx, int cy, int r) {
		g.drawOval(cx - r, cy - r, r * 2, r * 2);
	}

	/** Handle a mouse movement, refreshing appearance if necessary */
	public void mouseMoved(MouseEvent e) {
		refreshAppearance(e);
	}

	/**
	 * Show handles (and maybe hovering over handles) if necessary and repaint
	 * the canvas if there is a change.
	 */
	void refreshAppearance(MouseEvent e) {
		boolean previousShowHandles = showHandles;
		showHandles = isWithinCircle(e, x, y, 2 * radius);

		if (showHandles) {
			for (Handle handle : handles) {
				handle.mouseMoved(e);
			}
		}

		if (previousShowHandles ^ showHandles) {
			canvas.repaint();
		}
	}

	/**
	 * Handle a mouse click to erase an edge or node. Doesn't handle dragging.
	 * 
	 * @return Whether or not the click was handled
	 */
	public boolean eraseToolPressed(MouseEvent e) {

		// The node is reporting that it was clicked so that the canvas will
		// consider it for deletion when the mouse is released
		if (isClicked(e)) {
			return true;
		}

		if ((left != null)
				&& shouldDeleteEdge(e, leftHandle.getX(), leftHandle.getY(),
						left.topHandle.getX(), left.topHandle.getY())) {
			return true;
		}

		if ((right != null)
				&& shouldDeleteEdge(e, rightHandle.getX(), rightHandle.getY(),
						right.topHandle.getX(), right.topHandle.getY())) {
			return true;
		}

		return false;
	}

	/**
	 * Handle a mouse release for the erase tool
	 * 
	 * @param e
	 *            : the particular MouseEvent
	 */
	public void eraseToolReleased(MouseEvent e) {
		eraseApplicable(e);
	}

	/**
	 * Handle a mouse drag for the erase tool
	 * 
	 * @param e
	 *            : the particular MouseEvent
	 */
	public void eraseToolDragged(MouseEvent e) {
		eraseApplicable(e);
	}

	/**
	 * Erase the Node and/or its edges if the given MouseEvent intersects the
	 * Node or any of its edges.
	 * 
	 * @param e
	 *            : the particular MouseEvent
	 */
	private void eraseApplicable(MouseEvent e) {

		// Deleting the node
		if (isClicked(e)) {
			delete();
			return;
		}

		// Delete left edge IF node not deleted
		if (left != null) {

			// Endpoints of the edge
			if (shouldDeleteEdge(e, leftHandle.getX(), leftHandle.getY(),
					left.topHandle.getX(), left.topHandle.getY())) {
				setLeft(null);
				canvas.updateTree();
				canvas.repaint();
			}
		}

		// Delete right edge
		if (right != null) {

			// Endpoints of the edge
			if (shouldDeleteEdge(e, rightHandle.getX(), rightHandle.getY(),
					right.topHandle.getX(), right.topHandle.getY())) {
				setRight(null);
				canvas.updateTree();
				canvas.repaint();
			}
		}
	}

	/**
	 * Performs all the required options to ensure that the deletion of the Node
	 * leaves the tree in a consistent/stable state. Refreshes the tree
	 * appearance.
	 */
	public void delete() {
		// Deleting the root (if allowed)
		if (isRoot()) {

			// Cannot delete if there are 0 or 2 children
			if (!(left == null ^ right == null)) {
				canvas.getGUI()
						.showErrorMessage(
								"Root node can be deleted if and only if it has one child.",
								"Deletion Error");
				return;
			}

			// Set the new root
			(left != null ? left : right).setToRoot();
		}

		canvas.getNodes().remove(this);

		// Tell children they no longer have parents
		setLeft(null);
		setRight(null);

		// Tell parent it no longer has a child
		if (parent != null) {
			if (parent.left == this) {
				parent.setLeft(null);
			} else if (parent.right == this) {
				parent.setRight(null);
			}
		}

		canvas.updateTree();
		canvas.repaint();
		return;
	}

	/**
	 * Checks if a small circle surrounding the mouse click e intersects with a
	 * line segment with the coordinates (x1, y1, x2, y2).
	 * 
	 * @return True if the intersection holds and the edge should be deleted,
	 *         false otherwise
	 */
	private boolean shouldDeleteEdge(MouseEvent e, int x1, int y1, int x2,
			int y2) {

		// Center of the circle (mouse)
		int cx = e.getX();
		int cy = e.getY();

		int dx = x1 - x2;
		int dy = y1 - y2;
		double denom = 1.0 / (dx * dx + dy * dy);

		boolean delete = false;

		for (double t : new double[] { 0, 1,
				(dx * (cx - x2) + dy * (cy - y2)) * denom }) {
			delete = 0 <= t
					&& t <= 1
					&& Math.pow(t * x1 + (1 - t) * x2 - cx, 2)
							+ Math.pow(t * y1 + (1 - t) * y2 - cy, 2) <= DELETE_RADIUS_SQUARED;
			if (delete) {
				break;
			}
		}
		return delete;
	}

	static void reassignRoot(Node newRoot) {
		// this should always terminate, provided cycle checking is correct and
		// there aren't any...
		while (newRoot.parent != null) {
			newRoot = newRoot.parent;
		}
		newRoot.setToRoot();
	}

	/**
	 * Recursively creates a subtree with the specified number of nodes. All
	 * nodes are created with the same coordinates, so a subsequent call to
	 * canvas.layoutSubtree() with the returned subtree root is recommended.
	 * 
	 * @param numberOfNodes
	 *            the number of nodes to place in the subtree
	 * @param xPosition
	 *            the x coordinate of the nodes to be created
	 * @param yPosition
	 *            the y coordinate of the nodes to be created
	 * @return the root Node of the subtree
	 */
	public Node createSubtree(int numberOfNodes, int xPosition, int yPosition) {

		// Create and add a new node
		Node newNode = new Node(xPosition, yPosition, canvas);
		canvas.getNodes().add(newNode);

		// Calculate the height of the subtree
		int heightOfSubtree = 0;
		while (Math.pow(2, heightOfSubtree) <= numberOfNodes) {
			heightOfSubtree++;
		}

		// Calculate the number of nodes in the left subtree. Note that
		// (number of nodes in left subtree) >= (number of nodes in right
		// subtree), as left subtrees must be filled (up to height of
		// subtree restriction) before right subtrees
		int nodesInLeftSubtree = (int) Math.pow(2, heightOfSubtree - 2);
		int remainder = (numberOfNodes - 1) - (nodesInLeftSubtree * 2 - 1);
		while (remainder > 0
				&& nodesInLeftSubtree < Math.pow(2, heightOfSubtree - 1) - 1) {
			--remainder;
			++nodesInLeftSubtree;
		}

		// Recursively create left subtree, if necessary
		if (nodesInLeftSubtree > 0) {
			newNode.setLeft(createSubtree(nodesInLeftSubtree, xPosition,
					yPosition));
		}

		// Recursively create right subtree, if necessary
		if (numberOfNodes - 1 - nodesInLeftSubtree > 0) {
			newNode.setRight(createSubtree(numberOfNodes - 1
					- nodesInLeftSubtree, xPosition, yPosition));
		}

		// Return the root of the subtree
		return newNode;
	}

	/** Should be called before getContour() */
	public void prepareForLayout() {
		contour = null;
		for (Node node : children()) {
			if (node != null) {
				node.prepareForLayout();
			}
		}
	}

	/**
	 * Get the 'contour' of a node's subtree: the positions of the tree's
	 * outermost nodes. Each entry of the list is a two-element array containing
	 * the x-position relative to the root of the subtree of the outermost nodes
	 * at that level of depth, where the first element corresponds to the direct
	 * children. Here the x-position refers to a position in a grid for laying
	 * out the nodes, not the usual x within the canvas. These positions are
	 * calculated so that the tree can be packed together for an optimal layout.
	 * The method recursively calculates contours for its descendants to get the
	 * subtrees as close together as possible while keeping things symmetrical.
	 * Should be called after prepareForLayout().
	 */
	public List<int[]> getContour() {

		// If the contour hasn't already been calculated since
		// prepareForLayout() was called...
		if (contour == null) {

			childDistance = 1;

			// A leaf has an empty contour
			if (left == null && right == null) {
				return new ArrayList<>();
			}

			// The contour when the node has only one child - trivial
			// Left child only
			if (left != null && right == null) {
				childDistance = 1;
				for (int[] pair : left.getContour()) {
					pair[0] -= childDistance;
					pair[1] -= childDistance;
				}
				contour = left.getContour();
				contour.add(0, new int[] { -childDistance, -childDistance });

				// Right child only
			} else if (left == null && right != null) {
				childDistance = 1;
				for (int[] pair : right.getContour()) {
					pair[0] += childDistance;
					pair[1] += childDistance;
				}
				contour = right.getContour();
				contour.add(0, new int[] { childDistance, childDistance });
			} else {

				// Two children
				// Packing two subtrees close together
				Iterator<int[]> leftIter = left.getContour().iterator();
				Iterator<int[]> rightIter = right.getContour().iterator();
				while (leftIter.hasNext() && rightIter.hasNext()) {

					// The magical formula to find the minimum child distance
					// without overlapping
					childDistance = Math.max(childDistance,
							(leftIter.next()[1] - rightIter.next()[0]) / 2 + 1);
				}

				// Using the child distance to calculate a new contour by
				// combining the left and right contours
				contour = left.getContour();
				int[] pair;
				leftIter = contour.iterator();
				rightIter = right.getContour().iterator();

				// Tree depths where there are nodes from both subtrees
				while (leftIter.hasNext() && rightIter.hasNext()) {
					pair = leftIter.next();
					pair[0] -= childDistance;
					pair[1] = rightIter.next()[1] + childDistance;
				}

				// Tree depths where one of the subtrees is finished
				while (leftIter.hasNext()) {
					pair = leftIter.next();
					pair[0] -= childDistance;
					pair[1] -= childDistance;
				}
				while (rightIter.hasNext()) {
					pair = rightIter.next();
					contour.add(pair);
					pair[0] += childDistance;
					pair[1] += childDistance;
				}

				// Finally, the positions of the two children will be symmetric
				// around the root
				contour.add(0, new int[] { -childDistance, childDistance });
			}

		}
		return contour;
	}

	/**
	 * Returns whether this node is an ancestor of toFind. Useful for cycle
	 * checking.
	 */
	public boolean isAncestorOf(Node toFind) {
		return this == toFind || left != null && left.isAncestorOf(toFind)
				|| right != null && right.isAncestorOf(toFind);
	}

	/**
	 * Moves this node and its children into optimal position, usually for
	 * layout mode. Must call getContour() first to calculate childDistance.
	 * 
	 * @param newX
	 *            The x-position to move into.
	 * @param newY
	 *            The y-position to move into.
	 * @param xStep
	 *            The x-spacing in the grid being laid out on
	 * @param yStep
	 *            The y-spacing in the grid being laid out on
	 * @param step
	 *            The animation step that this method should insert actions
	 *            into, i.e. when the step is started this node will smoothly
	 *            move into place.
	 */
	public void optimiseLayout(int newX, int newY, int xStep, int yStep,
			AnimationStep step) {

		// Move this node into place when the animation step starts
		animateMove(newX, newY, LAYOUT_TIME, step);

		// Move the children into their places according to childDistance
		// earlier calculated
		if (left != null) {
			left.optimiseLayout(newX - xStep * childDistance, newY + yStep,
					xStep, yStep, step);
		}
		if (right != null) {
			right.optimiseLayout(newX + xStep * childDistance, newY + yStep,
					xStep, yStep, step);
		}

	}

	/**
	 * Insert actions to move this node to (targetX, targetY) into the given
	 * animation step.
	 */
	public void animateMove(int targetX, int targetY, int time,
			AnimationStep step) {
		step.addActions(new AnimationAction(time, x, targetX,
				Interpolator.S_SHAPED) {

			@Override
			public void updateAnimationValue(float value) {
				x = (int) value;
			}
		}, new AnimationAction(time, y, targetY, Interpolator.S_SHAPED) {

			@Override
			public void updateAnimationValue(float value) {
				y = (int) value;
			}
		});
	}

	/**
	 * Insert actions into the step to fade the fill color of the node from
	 * oldColor to newColor.
	 */
	public void animateSetColor(Color oldColor, Color newColor,
			AnimationStep step) {
		step.addActions(new AnimationAction(CHANGE_COLOR_TIME, oldColor
				.getRed(), newColor.getRed(), Interpolator.LINEAR) {

			@Override
			public void updateAnimationValue(float value) {

				fillColor = new Color((int) value, fillColor.getGreen(),
						fillColor.getBlue());
			}
		}, new AnimationAction(CHANGE_COLOR_TIME, oldColor.getGreen(), newColor
				.getGreen(), Interpolator.LINEAR) {

			@Override
			public void updateAnimationValue(float value) {

				fillColor = new Color(fillColor.getRed(), (int) value,
						fillColor.getBlue());
			}
		}, new AnimationAction(CHANGE_COLOR_TIME, oldColor.getBlue(), newColor
				.getBlue(), Interpolator.LINEAR) {

			@Override
			public void updateAnimationValue(float value) {

				fillColor = new Color(fillColor.getRed(), fillColor.getGreen(),
						(int) value);
			}
		});
	}

	/**
	 * Insert actions into the step to fade the fill color of the node from its
	 * current color to newColor.
	 */
	public void animateSetColor(Color newColor, AnimationStep step) {
		animateSetColor(fillColor, newColor, step);
	}

	public boolean hasContents() {
		return contents != NO_CONTENTS;
	}

	/**
	 * Whether this node was calculated to be in a position in the tree where it
	 * would be if had it been inserted. If false, the tree is not correctly
	 * sorted. Use after checkWithinSortedBounds() has been called and
	 * calculated if the node is in place.
	 */
	public boolean isInSortedPosition() {
		return inSortedPosition;
	}

	/**
	 * Use before checkSortedWithinBounds() to assume that all nodes are sorted
	 * before showing that they are not.
	 */
	public void resetInSortedPosition() {
		inSortedPosition = true;
	}

	/**
	 * Check if the contents of the node are in the correct place in a sorted
	 * tree based on the contents of its parents. The node and all its children
	 * must have contents between minValue and maxValue, not inclusive. The
	 * bounds are adjusted for the children to ensure that the binary search
	 * tree property is being obeyed.
	 */
	public boolean checkSortedWithinBounds(int minValue, int maxValue) {
		if (!hasContents()) {
			return true;
		}
		if (contents <= minValue || contents >= maxValue) {
			inSortedPosition = false;
			return false;
		}
		// Non-short-circuiting & operator intentionally used
		return (left == null || left
				.checkSortedWithinBounds(minValue, contents))
				& (right == null || right.checkSortedWithinBounds(contents,
						maxValue));
	}

	/** Height of leaf = 1. Height of null node = 0. */
	private static int calculateHeight(Node node) {
		return node == null ? 0 : 1 + Math.max(calculateHeight(node.left),
				calculateHeight(node.right));
	}

	/**
	 * 
	 * @return : the balance factor of the Node
	 */
	public int calculateBalanceFactor() {
		return calculateHeight(right) - calculateHeight(left);
	}

	/**
	 * 
	 * @return : the integer contents of the Node
	 */
	public int getContents() {
		return contents;
	}

	/** Animate the movement of a Node's edge. */
	public void animateMoveEdge(final Node oldChild,
			final AnimationStep setNewChildStep, final Node newChild) {
		animateMoveEdge(oldChild, setNewChildStep, newChild, oldChild == left);
	}

	/**
	 * Animate the movement of a Node's edge.
	 * 
	 * @param oldChild
	 *            : the old child of the Node
	 * @param setNewChildStep
	 *            : the AnimationStep of the animation
	 * @param newChild
	 *            : the new child of the Node
	 * @param isLeft
	 *            : whether or not the left child of the Node is being
	 *            reassigned
	 */
	public void animateMoveEdge(final Node oldChild,
			final AnimationStep setNewChildStep, final Node newChild,
			final boolean isLeft) {
		setNewChildStep.addActions(new InstantAnimationAction() {

			@Override
			public void updateAnimationValue(float value) {
				movingLeftEdge = isLeft;
				movingRightEdge = !isLeft;
				if (isLeft) {
					setLeft(newChild);
				} else {
					setRight(newChild);
				}
				int newX, newY, oldX, oldY;
				if (newChild == null) {
					newX = isLeft ? leftHandle.getX() : rightHandle.getX();
					newY = leftHandle.getY();
				} else {
					newX = newChild.topHandle.getX();
					newY = newChild.topHandle.getY();
				}
				if (oldChild == null) {
					oldX = isLeft ? leftHandle.getX() : rightHandle.getX();
					oldY = leftHandle.getY();
				} else {
					oldX = oldChild.topHandle.getX();
					oldY = oldChild.topHandle.getY();
				}
				setNewChildStep.addActions(new AnimationAction(MOVE_EDGE_TIME,
						oldX, newX, Interpolator.S_SHAPED) {

					@Override
					public void updateAnimationValue(float value) {
						movingEdgeX = (int) value;
					}
				}, new AnimationAction(MOVE_EDGE_TIME, oldY, newY,
						Interpolator.S_SHAPED) {

					@Override
					public void updateAnimationValue(float value) {
						movingEdgeY = (int) value;
					}
				});
			}
		});
	}

	/**
	 * 
	 * @param newNode
	 *            : the Node to which the contents of the calling Node will be
	 *            moved.
	 * @return : the AnimationStep pertaining to animating the movement of the
	 *         contents.
	 */
	public AnimationStep animateMoveContents(final Node newNode) {
		final AnimationStep step = new AnimationStep(canvas);
		step.addActions(new InstantAnimationAction() {

			@Override
			public void updateAnimationValue(float value) {
				movingContents = true;
				newNode.contents = NO_CONTENTS;
				step.addActions(new AnimationAction(MOVE_CONTENTS_TIME, x,
						newNode.getX(), Interpolator.S_SHAPED) {

					@Override
					public void updateAnimationValue(float value) {
						movingContentsX = (int) value;
					}
				}, new AnimationAction(MOVE_CONTENTS_TIME, y, newNode.getY(),
						Interpolator.S_SHAPED) {

					@Override
					public void updateAnimationValue(float value) {
						movingContentsY = (int) value;
					}
				});
			}
		});
		return step;
	}

	/** Reset variables pertaining to animating moving edges. */
	public void resetMovingEdges() {
		movingLeftEdge = false;
		movingRightEdge = false;
	}

	/** Reset variables pertaining to animating moving contents. */
	public void resetMovingContents() {
		movingContents = false;
	}

	/** Set the contents of the Node */
	public void setContents(int contents) {
		this.contents = contents;
	}

	/**
	 * 'Clear' the contents of this Node by setting the contents value to the
	 * NO_CONTENTS static integer. Painting of contents does not take place for
	 * this value.
	 */
	public void clearContents() {
		setContents(NO_CONTENTS);
	}

	/**
	 * Recursive method which assigns each Node in the subtree of the calling
	 * Node contents. The content values are all multiples of 10, starting at 10
	 * and ending at (number of Nodes in calling Node's tree)*10. The contents
	 * are assigned such that the tree is sorted. All previous contents values
	 * are overwritten.
	 */
	public int fillContents(int i) {
		if (left != null) {
			i = left.fillContents(i);
		}
		setContents(10 * i++);
		if (right != null) {
			i = right.fillContents(i);
		}
		return i;
	}
}
