// CSC3003S - Capstone Project - CHALK
// Alex Hall & Matthew Welham
// HLLALE006 & WLHMAT002

package tree;

/**
 * Class representing the Handle on the lower-left side of the Node which
 * represents the connection point between Nodes and their left child.
 */
class LeftHandle extends ChildHandle {

	LeftHandle(Node node) {
		super(node);
	}

	/**
	 * Return the x-coordinate of the LeftHandle's centre. Overrides Handle's
	 * method.
	 */
	@Override
	int getX() {
		return (int) Math.round(node.x - node.radius * SQRT_2_INV);
	}

	/** Returns a reference to the left child of the owning Node. */
	@Override
	Node getChild() {
		return node.left;
	}

	/** Sets the left child of the owning Node. */
	@Override
	void setChild(Node otherNode) {
		node.setLeft(otherNode);
	}

}
