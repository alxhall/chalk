// CSC3003S - Capstone Project - CHALK
// Alex Hall & Matthew Welham
// HLLALE006 & WLHMAT002

package tree;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;

/**
 * Abstract class representing one of the three 'handles' (the small circles on
 * the interface) which represent the connection points between Nodes and their
 * parent and children.
 */
abstract class Handle {

	/** The Node to which the Handle belongs */
	final Node node;

	Handle(Node node) {
		this.node = node;
	}

	private static final int HANDLE_RADIUS = 6;

	private final Color HOVER_COLOUR = new Color(255, 0, 0, 128);

	private boolean showHover = false;

	private boolean dragging = false;

	int newEdgeEndX;
	int newEdgeEndY;

	/**
	 * Determines how the Handle will be painted on the Canvas holding its
	 * owning Node.
	 */
	void paint(Graphics g) {
		if (showHover) {
			g.setColor(HOVER_COLOUR);
			Node.fillCircle(g, getX(), getY(), HANDLE_RADIUS);
		}
		g.setColor(Color.black);
		Node.drawCircle(g, getX(), getY(), HANDLE_RADIUS);
		if (dragging) {
			g.drawLine(getX(), getY(), newEdgeEndX, newEdgeEndY);
		}
	}

	/** How the Handle responds to mouse releases. */
	void mouseReleased(MouseEvent e) {

		// If the mouse has been dragged from this handle to a point outside
		// this handle:
		if (dragging && !isWithin(e)) {

			// checks whether Nodes should be connected or if a Node or Nodes
			// should potentially be created.
			if (Node.hoveringHandle != null
					&& connectToHandle(Node.hoveringHandle,
							Node.hoveringHandle.node, e)) {
				node.canvas.updateTree();
			} else {
				createNode();
			}

			refreshAppearance(e);
			node.refreshAppearance(e);
		}
		dragging = false;
	}

	/** Method to be inherited to define how Nodes are created. */
	abstract void createNode();

	/**
	 * Tries creating a new edge by connecting this handle to otherHandle.
	 * Returns false if they were incompatible and a new node can be created
	 * instead.
	 */
	abstract boolean connectToHandle(Handle otherHandle, Node otherNode,
			MouseEvent e);

	/**
	 * Shows and error if a cycle was created, and refreshes the appearance of
	 * the Handle and its owning Node.
	 */
	void showCycleError(MouseEvent e) {
		node.canvas.getGUI().showErrorMessage(
				"Creating this edge would result in the formation of a cycle.",
				"Cannot create cycle");
		dragging = false;
		refreshAppearance(e);
		node.refreshAppearance(e);
	}

	/** How the Handle responds to mouse dragging. */
	void mouseDragged(MouseEvent e) {
		newEdgeEndX = e.getX();
		newEdgeEndY = e.getY();
	}

	/** How the Handle responds to mouse presses. */
	boolean mousePressed(MouseEvent e) {
		if (isWithin(e)) {
			dragging = true;
			newEdgeEndX = e.getX();
			newEdgeEndY = e.getY();
			return true;
		}
		return false;
	}

	/**
	 * Returns whether a MouseEvent has coordinates which lie within the Handle.
	 */
	private boolean isWithin(MouseEvent e) {
		return Node.isWithinCircle(e, getX(), getY(), HANDLE_RADIUS);
	}

	/** Return the x-coordinate of the Handle's centre */
	abstract int getX();

	/** Return the y-coordinate of the Handle's centre */
	abstract int getY();

	/** How the Handle responds to mouse movement. */
	void mouseMoved(MouseEvent e) {
		refreshAppearance(e);
	}

	/** Update the appearance of the owning Node (and hence the Handle) */
	private void refreshAppearance(MouseEvent e) {
		boolean previousShowHover = showHover;
		showHover = isWithin(e);

		// Could add a rectangle clip for performance
		if (previousShowHover ^ showHover) {
			Node.hoveringHandle = showHover ? this : null;
			this.node.canvas.repaint();
		}
	}

}