// CSC3003S - Capstone Project - CHALK
// Alex Hall & Matthew Welham
// HLLALE006 & WLHMAT002

package gui;

import animation.AnimationSequence;

/** Tool that allows for the selection of a Node to delete */
// Most, if not all, method comments as per Tool and SelectNodeToAnimateTool.
public class SelectNodeToAnimateBSTDeleteTool extends SelectNodeToAnimateTool {

	// Singleton pattern
	static SelectNodeToAnimateBSTDeleteTool instance;

	SelectNodeToAnimateBSTDeleteTool(MainFrame frame) {
		super(frame);
		instance = this;
	}

	@Override
	AnimationSequence animation() {
		return frame.animationFactory.bstDeleteAnimation(activeNode);
	}

}
