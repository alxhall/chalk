package gui;

import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.RenderingHints;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import tree.Node;
import animation.AnimationStep;

/**
 * Class extending JPanel upon which the tree will be drawn. Contains all of the
 * Nodes created by the user and is responsible for the visual representation of
 * the tree
 */
public class Canvas extends JPanel implements ComponentListener {

	public static final BasicStroke DEFAULT_STROKE = new BasicStroke(2);

	private static final int DEFAULT_ROWS = 6;
	private static final int DEFAULT_COLUMNS = 11;

	private static final long serialVersionUID = -4598947966326902012L;

	/** Handles saving, loading and exporting of trees. Used from MainFrame */
	private FileHandler fileHandler;

	/** The list of Nodes on the Canvas */
	private List<Node> nodes = new ArrayList<>();

	private Node root;

	private MainFrame frame;

	/** Used for save prompts */
	protected boolean treeChanged = false;

	private boolean shiftKeyDown = false;

	private int lastKeyCode = 1; // initialised to non-zero value

	/**
	 * Whether or not a keyboard shortcut is being processed. Only one should be
	 * processed at any given time.
	 */
	private boolean processingKeyPressed = false;

	/**
	 * The constructor essentially defines all of the Listeners (component,
	 * mouse, keyboard) applicable to the JPanel
	 */
	Canvas(MainFrame frame_) {

		this.frame = frame_;

		addComponentListener(this);

		// listeners for mouse presses and releases (clicks)
		addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent e) {
				frame.selectedTool.mousePressed(e);
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				frame.selectedTool.mouseReleased(e);
			}

		});

		// listeners for mouse movement (move and drag)
		addMouseMotionListener(new MouseAdapter() {

			@Override
			public void mouseDragged(MouseEvent e) {
				frame.selectedTool.mouseDragged(e);
			}

			@Override
			public void mouseMoved(MouseEvent e) {
				frame.selectedTool.mouseMoved(e);
			}

		});

		// http://stackoverflow.com/questions/10064296/check-whether-shift-key-is-pressed
		// keyboard listener for determining keyboard shortcuts
		KeyboardFocusManager.getCurrentKeyboardFocusManager()
				.addKeyEventDispatcher(new KeyEventDispatcher() {
					public boolean dispatchKeyEvent(KeyEvent e) {
						shiftKeyDown = e.isShiftDown();
						boolean ctrlKeyDown = e.isControlDown();

						/*
						 * Pressing (and holding down) Shift will result in a
						 * single key code of 16 registered. Upon releasing,
						 * another key code of 16 is registered (overall: 16
						 * 16). For a character, however, pressing and releasing
						 * registers 3 key codes (x 0 x), where x is the key
						 * code of the character. Hence it is possible to
						 * determine when a key is released by checking if the
						 * previous key code checked was 0.
						 */
						if (lastKeyCode == 0) {
							if (!processingKeyPressed) {
								try {
									processingKeyPressed = true;
									if (shiftKeyDown) {
										if (ctrlKeyDown
												&& (e.getKeyCode() == 83)) {

											// Ctrl + Shift + S, save tree as...
											fileHandler.saveTree(false);
										}
									} else if (ctrlKeyDown) {
										if (e.getKeyCode() == 78) {

											// Ctrl + N, create new tree
											createNewTree();
										} else if (e.getKeyCode() == 83) {

											// Ctrl + Shift, save tree
											fileHandler.saveTree(true);
										} else if (e.getKeyCode() == 79) {

											// Ctrl + O, load tree
											fileHandler.loadTree();
										}
									} else if (e.isAltDown()) {
										if (e.getKeyCode() == 76) {

											// Alt + L, toggle layout mode
											frame.toggleLayoutMode();
										}
									} else if (((e.getKeyCode() == 27) // Esc
											&& (frame.selectedTool instanceof SelectNodeToAnimateTool))
											|| (e.getKeyCode() == 32)) {
										frame.setSelectedTool(DrawTool.instance);
									} else if (e.getKeyCode() == 69) {

										// E, select erase tool
										frame.setSelectedTool(EraseTool.instance);
									} else if (e.getKeyCode() == 77) {

										// M, select Move tool
										frame.setSelectedTool(MoveTool.instance);
									} else if (e.getKeyCode() == 67) {

										// C, select Edit (contents) tool
										frame.setSelectedTool(EditContentsTool.instance);
									}
								} finally {
									processingKeyPressed = false;
								}
							}
						}

						lastKeyCode = e.getKeyCode();
						return false;
					}
				});

		fileHandler = new FileHandler(this);
	}

	/** Return the FileHandler of this Canvas */
	public FileHandler getFileHandler() {
		return fileHandler;
	}

	/** Return the Canvas' list of Nodes */
	public List<Node> getNodes() {
		return nodes;
	}

	/**
	 * Overridden JPanel method. Responsible for painting all nodes, their edges
	 * and their contents respectively
	 */
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2 = (Graphics2D) g;

		g2.setStroke(DEFAULT_STROKE);
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);

		for (Node node : nodes) {
			node.paintNode(g2);
		}
		for (Node node : nodes) {
			node.paintEdges(g2);
		}
		for (Node node : nodes) {
			node.paintContents(g2);
		}

	}

	/** Return a reference to the root Node of the Canvas */
	public Node getRoot() {
		return root;
	}

	/** Set the root Node of the Canvas */
	public void setRoot(Node root) {
		if (root == null) {
			throw new NullPointerException(
					"Non null root required at all times");
		}
		this.root = root;
	}

	/**
	 * Returns whether or not the tree is connected. Effectively checks that the
	 * only node without a parent is the root
	 */
	public boolean isTreeConnected() {
		boolean connected = true;
		for (Node node : nodes) {
			boolean isRoot = node == root;
			boolean hasParent = node.getParent() != null;
			connected &= (hasParent || isRoot);
		}
		return connected;
	}

	/**
	 * Overridden JPanel method. Creates the root Node in the case of the
	 * application just having started. Otherwise centres the root Node if it is
	 * the only Node on the Canvas.
	 */
	@Override
	public void componentResized(ComponentEvent e) {

		// Adding the first node when the size of the canvas is determined
		if (nodes.size() == 0) {
			createRoot();
			nodes.add(root);
			repaint();
			treeChanged = false;
			resetNodeColors();
		} else if (nodes.size() == 1) {
			root.move(getWidth() / 2, getRootY());
			repaint();
			treeChanged = false;
			resetNodeColors();
		}
	}

	/** Creates a root Node at the top-centre of the Canvas */
	private void createRoot() {
		root = new Node(getWidth() / 2, getRootY(), this);
	}

	/**
	 * Returns the y-coordinate of where the root would ideally be located
	 */
	private int getRootY() {
		return (int) getHeight() / (DEFAULT_ROWS + 1);
	}

	/**
	 * Clears the current tree and leaves the Canvas with a single root Node,
	 * provided the user chose to save the previous tree if unsaved changes had
	 * been made.
	 */
	public void createNewTree() {

		if (fileHandler
				.askToSave(
						"Would you like to save your current tree before starting a new one?",
						"Save before creating?")) {

			nodes.clear();
			createRoot();
			nodes.add(root);
			setRoot(root);
			treeChanged = false;
			resetNodeColors();

			repaint();
		}
	}

	// Component listener methods

	@Override
	public void componentMoved(ComponentEvent e) {
	}

	@Override
	public void componentShown(ComponentEvent e) {
	}

	@Override
	public void componentHidden(ComponentEvent e) {
	}

	/**
	 * Updates the tree to reflect its current state. This involves notifying
	 * the tree that it has been changed, 'optimising' the layout of the tree if
	 * necessary, updating its sorted state and resetting Node colours.
	 */
	public void updateTree() {
		treeChanged = true;
		if (shouldLayoutTree()) {
			layoutSubtree(root, 0, 0);
		}
		isTreeSorted();
		resetNodeColors();
	}

	/**
	 * Returns whether the tree should have its layout 'optimised'. This depends
	 * on whether the tree is connected and whether the application is in Layout
	 * Mode.
	 */
	public boolean shouldLayoutTree() {
		return isTreeConnected() && frame.isLayoutMode();
	}

	/**
	 * Animates the 'optimisation' of a subtree's layout.
	 * 
	 * @param subtreeRoot
	 *            : the root of the subtree to layout
	 * @param xPosition
	 * @param yPosition
	 */
	public void layoutSubtree(Node subtreeRoot, int xPosition, int yPosition) {

		// resets the contour of each Node on the Canvas
		subtreeRoot.prepareForLayout();

		// recalculates contour values for each Node on the Canvas
		List<int[]> contour = subtreeRoot.getContour();

		// calculate the leftMost and rightMost Node positions.
		int leftMost = 0;
		int rightMost = 0;
		for (int[] pair : contour) {
			leftMost = Math.min(leftMost, pair[0]);
			rightMost = Math.max(rightMost, pair[1]);
		}

		// calculate number of rows, and 'delta y' values
		int numRows = Math.max(DEFAULT_ROWS, contour.size() + 1);
		int yStep = Math.round(getHeight() / (numRows + 1));

		// calculate number of columns, and 'delta x' values
		int numColumns = Math.max(DEFAULT_COLUMNS, rightMost - leftMost + 1);
		int xStep = Math.round(getWidth() / (numColumns + 1));

		// if we are laying out from the root, calculate the optimum position
		// for the root.
		if (subtreeRoot.isRoot()) {
			if (rightMost > DEFAULT_COLUMNS / 2) {
				xPosition = getWidth() - (1 + rightMost) * xStep;
			} else if (leftMost < -DEFAULT_COLUMNS / 2) {
				xPosition = (1 + Math.abs(leftMost)) * xStep;
			} else {
				xPosition = getWidth() / 2;
			}
			yPosition = yStep;
		}

		AnimationStep step = new AnimationStep(this);
		subtreeRoot.optimiseLayout(xPosition, yPosition, xStep, yStep, step);
		step.start();
	}

	/** Resets the colours of all Nodes on the Canvas, then repaints. */
	public void resetNodeColors() {
		for (Node node : nodes) {
			node.resetColor();
		}
		repaint();
	}

	/** Returns a reference to the MainFrame frame of this Canvas. */
	public MainFrame getGUI() {
		return frame;
	}

	/**
	 * Sets the state of the tree to reflect that it has been changed. For
	 * save-prompt purposes.
	 */
	public void changeTree() {
		treeChanged = true;
	}

	/** Returns whether the Shift key is being held down */
	public boolean isShiftKeyDown() {
		return shiftKeyDown;
	}

	/** Returns whether every Node on the canvas has contents */
	public boolean allNodesHaveContents() {
		for (Node node : nodes) {
			if (!node.hasContents()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Resets each Node's knowledge of whether it is sorted, recalculates this
	 * value for each Node in the root-connected tree and returns whether the
	 * root-connected tree is in a sorted state.
	 */
	public boolean isTreeSorted() {
		for (Node node : nodes) {
			node.resetInSortedPosition();
		}
		return root.checkSortedWithinBounds(Integer.MIN_VALUE,
				Integer.MAX_VALUE);
	}

	/**
	 * Method which assigns each Node in the root-connected tree contents by
	 * calling a the recursive Node method of the same name on the root Node.
	 * The content values are all multiples of 10, starting at 10 and ending at
	 * (number of Nodes in root-connected tree)*10. The contents are assigned
	 * such that the tree is sorted. All previous contents values are
	 * overwritten.
	 */
	public void fillContents() {
		root.fillContents(1);
		updateTree();
	}

	/** Calls the hideHandles Node method for each Node on the Canvas. */
	public void hideHandles() {
		for (Node node : nodes) {
			node.hideHandles();
		}
	}

}
