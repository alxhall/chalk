// CSC3003S - Capstone Project - CHALK
// Alex Hall & Matthew Welham
// HLLALE006 & WLHMAT002

package gui;

import java.awt.Cursor;
import java.awt.event.MouseEvent;

import tree.Node;

/**
 * Implementation of a 'general purpose' Tool. Can create and move nodes, as
 * well as act as EraseTool if the right mouse button is used.
 */
// Most, if not all, method comments as per Tool.
class DrawTool extends Tool {

	private static final Cursor CURSOR = new Cursor(Cursor.DEFAULT_CURSOR);

	// Singleton pattern
	static DrawTool instance;

	int mouseButtonDown;

	DrawTool(MainFrame frame) {
		super(frame);
		instance = this;
	}

	/** Act as EraseTool if right mouse button down, else act as DrawTool. */
	@Override
	boolean pressed(Node node, MouseEvent e) {
		mouseButtonDown = e.getButton();
		if (mouseButtonDown == MouseEvent.BUTTON3) {
			return EraseTool.instance.pressed(node, e);
		} else {
			return node.drawToolPressed(e);
		}
	}

	/** Act as EraseTool if right mouse button down, else act as DrawTool. */
	@Override
	void mouseReleased(MouseEvent e) {
		if (mouseButtonDown == MouseEvent.BUTTON3) {
			EraseTool.instance.mouseReleased(e);
		} else if (activeNode != null) {
			activeNode.drawToolReleased(e);
			repaint();
		}
		mouseButtonDown = MouseEvent.BUTTON1;
	}

	/** Act as EraseTool if right mouse button down, else act as DrawTool. */
	@Override
	void dragged(MouseEvent e) {
		if (mouseButtonDown == MouseEvent.BUTTON3) {
			EraseTool.instance.dragged(e);
		} else {
			if (activeNode != null) {
				activeNode.drawToolDragged(e);
				repaint();
			}
		}
	}

	/** Call mouseMoved on each Node of the Canvas. Used for showing Handles. */
	@Override
	void moved(MouseEvent e) {
		for (Node node : nodes()) {
			node.mouseMoved(e);
		}
	}

	@Override
	int toolbarIndex() {
		return 0;
	}

	@Override
	Cursor cursor() {
		return CURSOR;
	}

}
