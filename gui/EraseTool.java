// CSC3003S - Capstone Project - CHALK
// Alex Hall & Matthew Welham
// HLLALE006 & WLHMAT002

package gui;

import java.awt.Cursor;
import java.awt.event.MouseEvent;

import tree.Node;

/** Tool that erases Nodes and/or their edges. */
// Most, if not all, method comments as per Tool.
class EraseTool extends Tool {

	private static final Cursor CURSOR = new Cursor(Cursor.CROSSHAIR_CURSOR);
	
	// Singleton pattern
	static EraseTool instance;

	EraseTool(MainFrame frame) {
		super(frame);
		instance = this;
	}

	@Override
	boolean pressed(Node node, MouseEvent e) {
		return node.eraseToolPressed(e);
	}

	@Override
	void mouseReleased(MouseEvent e) {
		if (activeNode != null) {
			activeNode.eraseToolReleased(e);
			repaint();
		}
	}
	
	@Override
	void dragged(MouseEvent e) {
		Node node;
		for (int i = nodes().size() - 1; i >= 0; --i) {
			node = nodes().get(i);
			node.eraseToolDragged(e);
		}
	}

	@Override
	int toolbarIndex() {
		return 1;
	}

	@Override
	Cursor cursor() {
		return CURSOR;
	}
}
