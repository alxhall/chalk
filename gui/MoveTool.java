// CSC3003S - Capstone Project - CHALK
// Alex Hall & Matthew Welham
// HLLALE006 & WLHMAT002

package gui;

/** Tool that moves Nodes (and their subtrees). */
// Most, if not all, method comments as per Tool.
import java.awt.Cursor;
import java.awt.event.MouseEvent;

import tree.Node;

class MoveTool extends Tool {

	private static final Cursor CURSOR = new Cursor(Cursor.MOVE_CURSOR);
	
	// Singleton pattern
	static MoveTool instance;

	MoveTool(MainFrame frame) {
		super(frame);
		instance = this;
	}

	@Override
	boolean pressed(Node node, MouseEvent e) {
		return node.moveToolPressed(e);
	}

	@Override
	void mouseReleased(MouseEvent e) {
		if (activeNode != null) {
			activeNode.moveToolReleased(e);
			repaint();
		}
	}

	@Override
	void dragged(MouseEvent e) {
		if (activeNode != null) {
			activeNode.moveToolDragged(e);
			repaint();
		}
	}

	@Override
	int toolbarIndex() {
		return 3;
	}

	@Override
	Cursor cursor() {
		return CURSOR;
	}

}
