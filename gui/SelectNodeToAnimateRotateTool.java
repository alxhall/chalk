// CSC3003S - Capstone Project - CHALK
// Alex Hall & Matthew Welham
// HLLALE006 & WLHMAT002

package gui;

import animation.AnimationSequence;

/**
 * Tool that allows for the selection of an imbalanced Node about which to
 * perform a rotation animation.
 */
// Most, if not all, method comments as per Tool and SelectNodeToAnimateTool.
public class SelectNodeToAnimateRotateTool extends SelectNodeToAnimateTool {

	// Singleton pattern
	static SelectNodeToAnimateRotateTool instance;

	SelectNodeToAnimateRotateTool(MainFrame frame) {
		super(frame);
		instance = this;
	}

	@Override
	AnimationSequence animation() {
		return frame.animationFactory.rotateAnimation(activeNode);
	}

}
