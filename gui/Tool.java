// CSC3003S - Capstone Project - CHALK
// Alex Hall & Matthew Welham
// HLLALE006 & WLHMAT002

package gui;

import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.util.List;

import tree.Node;

/**
 * Abstract class representing a tree-manipulation tool of a MainFrame. Tool and
 * all classes which inherit it basically deal with how mouse events affect the
 * list of Nodes belonging to the Canvas of the MainFrame.
 */
abstract class Tool {

	static Node activeNode;

	final MainFrame frame;

	Tool(MainFrame frame) {
		this.frame = frame;
	}

	/**
	 * Handles mouse presses (mouse down) over the Canvas. Calls the overridden
	 * 'pressed' method for the given implementation of Tool on each Node in the
	 * Canvas of frame.
	 */
	void mousePressed(MouseEvent e) {
		if (checkNoAnimationInProgress()) {
			Node node;
			boolean handled = false;
			for (int i = nodes().size() - 1; i >= 0 && !handled; i--) {
				node = nodes().get(i);

				// calls the inherited version of 'pressed' on each
				// node, depending on the specific instance of Tool.
				handled = pressed(node, e);

				if (handled) {
					activeNode = node;
				}
			}
		}
	}

	/**
	 * Handles mouse releases (mouse up) over the Canvas belonging to frame.
	 */
	void mouseReleased(MouseEvent e) {
	}

	/**
	 * 
	 * @param node
	 *            : the Node potentially pressed
	 * @param e
	 *            : the mouse event
	 * @return the result of a call to the appropriate Node method given the
	 *         implementation of Tool.
	 */
	abstract boolean pressed(Node node, MouseEvent e);

	List<Node> nodes() {
		return frame.getCanvas().getNodes();
	}

	/** Invoke the repaint method of the Canvas belonging to MainFrame frame. */
	void repaint() {
		frame.getCanvas().repaint();
	}

	/** Handles mouse drags over the Canvas. */
	void mouseDragged(MouseEvent e) {
		if (checkNoAnimationInProgress()) {
			dragged(e);
		}
	}

	/** Involves actions that take place when the mouse is dragged. */
	void dragged(MouseEvent e) {
	}

	/** Handles mouse movements (not dragged) over the Canvas. */
	void mouseMoved(MouseEvent e) {
		if (frame.currentAnimation == null) {
			moved(e);
		}
	}

	/** Involves actions that take place when the mouse is moved. */
	void moved(MouseEvent e) {
	}

	/** Returns whether MainFrame frame is busy processing an animation. */
	private boolean checkNoAnimationInProgress() {
		if (frame.currentAnimation != null) {
			frame.showErrorMessage(
					"The tree cannot be modified while an animation is in progress.",
					"Animation in progress");
			return false;
		}
		return true;
	}

	/** Returns the integer index of the tool (on the toolbar). */
	abstract int toolbarIndex();

	/** Method to be inherited. Returns the Cursor of the tool. */
	abstract Cursor cursor();

}
