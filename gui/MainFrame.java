// CSC3003S - Capstone Project - CHALK
// Alex Hall & Matthew Welham
// HLLALE006 & WLHMAT002

package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import java.io.File;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.KeyStroke;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;

import animation.AnimationFactory;
import animation.AnimationSequence;

public class MainFrame extends JFrame {

	private static final String PLAY_TOOLTIP = "Gradually play through each step of the animation until completion";

	static final String DEFAULT_TITLE = "Tree Visualization";

	private static final Color SELECTED_TOOL_COLOR = Color.red;

	private static final Color TOOLBAR_COLOUR = Color.lightGray;

	private static final long serialVersionUID = -1616367561107980353L;

	private static final int WINDOW_WIDTH = 800;
	private static final int WINDOW_HEIGHT = 750;

	private static final int BUTTON_SIZE = 24;

	private Canvas canvas = new Canvas(this);

	private JPanel toolbar = new JPanel();

	AnimationFactory animationFactory = new AnimationFactory(this);

	Tool selectedTool = new DrawTool(this);

	private JCheckBoxMenuItem layoutModeMenuItem;
	private JCheckBoxMenuItem sortedModeMenuItem;
	private JCheckBoxMenuItem highlightUnbalancedModeMenuItem;
	private JCheckBoxMenuItem showBalanceFactorsMenuItem;

	private boolean playingAnimation = false;
	AnimationSequence currentAnimation;

	private final ImageIcon playIcon;
	private final ImageIcon pauseIcon;

	private JButton rewindButton;
	private JButton playPauseButton;
	private JButton nextStepButton;
	private JButton fastForwardButton;

	private JSpinner numNodesToCreate;

	private Cursor nodeToAnimateCursor;

	private final ActionListener repaintListener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			canvas.repaint();
		}
	};

	/**
	 * stores the user-selected state of layout mode prior to animations. This
	 * state is selected when an animation is cancelled
	 */
	private boolean layoutModeState = false;

	/** Run the program by creating a new MainFrame */
	public static void main(String[] args) {
		new MainFrame();
	}

	/** All the components are initialised within this constructor. */
	private MainFrame() {
		setTitle(DEFAULT_TITLE);
		setSize(WINDOW_WIDTH, WINDOW_HEIGHT);

		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

		// window listener for close operation
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				closeApplication();
			}
		});

		// Initialise tool static instances
		new EraseTool(this);
		new EditContentsTool(this);
		new MoveTool(this);
		new SelectNodeToAnimateRotateTool(this);
		new SelectNodeToAnimateBSTDeleteTool(this);

		// --- GUI contents: menu bar, toolbar, and canvas

		// The menu bar
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu fileMenu = new JMenu("File");
		menuBar.add(fileMenu);

		// New tree item
		addMenuItem(fileMenu, "New", new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				canvas.createNewTree();
			}
		});

		// Load tree item
		addMenuItem(fileMenu, "Load", new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				canvas.getFileHandler().loadTree();
			}
		});

		// Save tree item
		addMenuItem(fileMenu, "Save", new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				canvas.getFileHandler().saveTree(true);
			}
		});

		// Save As... tree item
		addMenuItem(fileMenu, "Save As...", new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				canvas.getFileHandler().saveTree(false);
			}
		});

		// Export... item
		addMenuItem(fileMenu, "Export...", new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				canvas.getFileHandler().exportImage();
			}
		});

		fileMenu.addSeparator();

		// Exit item
		addMenuItem(fileMenu, "Exit", new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				closeApplication();
			}
		});

		JMenu modeMenu = new JMenu("Modes");
		menuBar.add(modeMenu);

		// Layout mode item
		layoutModeMenuItem = addCheckboxMenuItem(modeMenu, "Layout mode",
				new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						if (isLayoutMode()) {
							canvas.updateTree();
							canvas.repaint();
						}
					}
				});

		// Sorted mode item
		sortedModeMenuItem = addCheckboxMenuItem(modeMenu, "Sorted mode",
				repaintListener);

		// Highlight unbalanced nodes menu item
		highlightUnbalancedModeMenuItem = addCheckboxMenuItem(modeMenu,
				"Highlight unbalanced nodes", repaintListener);

		// Show balance factors menu item
		showBalanceFactorsMenuItem = addCheckboxMenuItem(modeMenu,
				"Show balance factors", repaintListener);

		JMenu animationsMenu = new JMenu("Animations");
		menuBar.add(animationsMenu);

		// Animate find item
		addMenuItem(animationsMenu, "Animate find", new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				if (isTreeConsistentForAnimation()) {

					String toFindString = (String) JOptionPane.showInputDialog(
							MainFrame.this,
							"Enter the contents of the node to find:",
							"Find node", JOptionPane.PLAIN_MESSAGE, null, null,
							"");

					// If the user entered something...
					if (toFindString != null && toFindString.length() > 0) {
						try {
							startAnimation(animationFactory
									.findAnimation(Integer
											.parseInt(toFindString)));
						} catch (NumberFormatException ex) {
							showErrorMessage(
									"Contents of node must be an integer value.",
									"Find error");
							actionPerformed(e);
						}

					}
				}
			}

		});

		// Animate insert item
		addMenuItem(animationsMenu, "Animate simple BST insert",
				new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {

						if (isTreeConsistentForAnimation()) {

							String toFindString = (String) JOptionPane
									.showInputDialog(
											MainFrame.this,
											"Enter the contents of the node to insert:",
											"Insert node",
											JOptionPane.PLAIN_MESSAGE, null,
											null, "");

							// If the user entered something...
							if (toFindString != null
									&& toFindString.length() > 0) {
								try {
									startAnimation(animationFactory
											.bstInsertAnimation(Integer
													.parseInt(toFindString)));
								} catch (NumberFormatException ex) {
									showErrorMessage(
											"Contents of node must be an integer value.",
											"Insert error");
									actionPerformed(e);
								}

							}
						}
					}

				});

		// Animate BST delete item
		addMenuItem(
				animationsMenu,
				"Animate simple BST delete",
				showSelectNodeForAnimationMessage(
						"Animate delete",
						"Click on a node to animate its deletion, or press Esc to cancel.",
						SelectNodeToAnimateBSTDeleteTool.instance));

		// Animate rotate item
		addMenuItem(
				animationsMenu,
				"Animate rotate",
				showSelectNodeForAnimationMessage(
						"Animate rotate",
						"Click on a node with balance factor less than -1 or greater than 1 to rotate it so that its subtree becomes balanced,\nor press Esc to cancel.",
						SelectNodeToAnimateRotateTool.instance));

		JMenu helpMenu = new JMenu("Help");
		menuBar.add(helpMenu);

		// User manual item - opens up the user manual
		addMenuItem(helpMenu, "User Manual", new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				File userManual = new File("resources/UserManual.pdf");
				if (!userManual.exists()) {
					showErrorMessage(
							"Could not find 'resources/UserManual.pdf'",
							"User Manual not found");
				}
				try {
					Desktop.getDesktop().open(userManual);
				} catch (IOException e1) {
					e1.printStackTrace();
					showErrorMessage("Could not open user manual.",
							"Error opening User Manual");
				}
			}
		});

		getContentPane().add(toolbar, BorderLayout.NORTH);
		toolbar.setBorder(BorderFactory.createLineBorder(Color.darkGray));
		toolbar.setLayout(new FlowLayout(FlowLayout.LEFT));
		toolbar.setBackground(TOOLBAR_COLOUR);

		// Tool buttons
		newToolbarIconButton("/resources/images/cursor.png", DrawTool.instance,
				"Draw Tool");
		newToolbarIconButton("/resources/images/eraser.png",
				EraseTool.instance, "Eraser Tool");
		newToolbarIconButton("/resources/images/edit_contents.png",
				EditContentsTool.instance, "Node-Contents Editor Tool");
		newToolbarIconButton("/resources/images/move.png", MoveTool.instance,
				"Movement Tool");

		// Separator between tools and animation buttons
		addToolbarSeparator();

		// -- Animation interface

		// button for cancelling animations
		rewindButton = newToolbarIconButton("/resources/images/rewind.png",
				new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						currentAnimation.cancel();
						canvas.getFileHandler().loadTemporary();
						layoutModeMenuItem.setSelected(layoutModeState);
					}
				}, "Stop the animation and revert to the pre-animation state");

		// button for playing or pausing animations
		playPauseButton = newToolbarIconButton("/resources/images/play.png");
		playIcon = new ImageIcon(new ImageIcon(getClass().getResource(
				"/resources/images/play.png")).getImage().getScaledInstance(
				BUTTON_SIZE, BUTTON_SIZE, Image.SCALE_SMOOTH));
		pauseIcon = new ImageIcon(new ImageIcon(getClass().getResource(
				"/resources/images/pause.png")).getImage().getScaledInstance(
				BUTTON_SIZE, BUTTON_SIZE, Image.SCALE_SMOOTH));
		playPauseButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (playingAnimation) {
					playPauseButton.setIcon(playIcon);
					playPauseButton.setToolTipText(PLAY_TOOLTIP);
					currentAnimation.pause();
				} else {
					playPauseButton.setIcon(pauseIcon);
					playPauseButton.setToolTipText("Pause animation");
					currentAnimation.play();
				}
				playingAnimation = !playingAnimation;
			}
		});
		playPauseButton.setToolTipText(PLAY_TOOLTIP);

		// button for performing the next step of an animation
		nextStepButton = newToolbarIconButton("/resources/images/next.png",
				new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						if (currentAnimation != null) {
							currentAnimation.nextStepClicked();
						}
					}
				}, "Perform the next step of the animation");

		// button for fast-forwarding through an animation
		fastForwardButton = newToolbarIconButton(
				"/resources/images/fast_forward.png", new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						if (currentAnimation != null) {
							currentAnimation.fastForward();
						}
					}
				}, "Quickly complete the animation");

		// disable the animation buttons on program start - no animation in
		// progress
		setAnimationButtonsEnabled(false);

		// The canvas
		getContentPane().add(canvas, BorderLayout.CENTER);

		// Separator between animation buttons and nodesToCreate JSpinner
		addToolbarSeparator();

		SpinnerModel spinnerModel = new SpinnerNumberModel(1, 1, 31, 1);
		// Spinner to choose how many nodes to create on each node create
		numNodesToCreate = new JSpinner(spinnerModel);
		((JSpinner.DefaultEditor) numNodesToCreate.getEditor()).getTextField()
				.setEditable(false); // prevent editing of spinner through
										// keyboard
		numNodesToCreate.setToolTipText("Number of nodes to create at once");
		toolbar.add(numNodesToCreate);

		// Separator between nodesToCreate JSpinner and fill-contents button
		addToolbarSeparator();

		// button for filling in the contents of all nodes in the root-connected
		// tree
		newToolbarIconButton("/resources/images/fill_contents.png",
				new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						canvas.fillContents();
					}
				}, "Automatically fill every node of the tree with contents");

		updateToolbar();

		// generate non-standard cursors
		generateCustomCursors();

		// ensure tool button in focus isn't pressed when spacebar is pressed.
		// Spacebar must select draw tool.
		InputMap buttonMap = (InputMap) UIManager.get("Button.focusInputMap");
		buttonMap.put(KeyStroke.getKeyStroke("pressed SPACE"), "none");
		buttonMap.put(KeyStroke.getKeyStroke("released SPACE"), "none");

		// prevent resizing beyond a certain limit. Prevents tools being cut off
		setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT / 2));

		setVisible(true);
	}

	/**
	 * Creates a new JButton applicable to the toolbar
	 * 
	 * @param imagePath
	 *            : path to the icon
	 * @param actionListener
	 *            : the ActionListener to add to the button
	 * @param toolTip
	 *            : the ToolTip text to assign to the button
	 * @return a reference to the created JButton
	 */
	private JButton newToolbarIconButton(String imagePath,
			ActionListener actionListener, String toolTip) {
		JButton button = newToolbarIconButton(imagePath);
		button.setToolTipText(toolTip);
		button.addActionListener(actionListener);
		return button;
	}

	/**
	 * Add a bit of space to the toolbar. For separating collections of buttons
	 * or other components.
	 */
	private void addToolbarSeparator() {
		JPanel separator = new JPanel() {
			// Just to get rid of the warning
			private static final long serialVersionUID = 2L;

			@Override
			public Dimension getPreferredSize() {
				return new Dimension(BUTTON_SIZE * 2, BUTTON_SIZE);
			}
		};
		separator.setBackground(TOOLBAR_COLOUR);
		toolbar.add(separator);
	}

	/**
	 * Creates a new JButton applicable to the toolbar, associated with a
	 * particular Tool.
	 * 
	 * @param imagePath
	 *            : path to the icon
	 * @param tool
	 *            : the Tool to be selected when the button is pressed
	 * @param toolTip
	 *            : the ToolTip text to assign to the button
	 * @return a reference to the created JButton
	 */
	private JButton newToolbarIconButton(String imagePath, final Tool tool,
			String toolTip) {
		return newToolbarIconButton(imagePath, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				selectedTool = tool;
				MainFrame.this.updateToolbar();
			}
		}, toolTip);
	}

	/**
	 * Create a JMenuItem and add it to a JMenu
	 * @param menu : the JMenu to which to add the created JMenuItem
	 * @param text : the text of the JMenuItem
	 * @param listener : the ActionListener of the JMenuItem
	 * @param checkbox : whether or not the JMenuItem is a JCheckBoxMenuItem
	 * @return : the created JMenuItem
	 */
	private JMenuItem addMenuItem(JMenu menu, String text,
			ActionListener listener, boolean checkbox) {
		JMenuItem menuItem = checkbox ? new JCheckBoxMenuItem(text)
				: new JMenuItem(text);
		menu.add(menuItem);
		menuItem.addActionListener(listener);
		return menuItem;
	}

	/**
	 * Create a JMenuItem and add it to a JMenu
	 * @param menu : the JMenu to which to add the created JMenuItem
	 * @param text : the text of the JMenuItem
	 * @param listener : the ActionListener of the JMenuItem
	 * @return : the created JMenuItem
	 */
	private JMenuItem addMenuItem(JMenu menu, String text,
			ActionListener listener) {
		return addMenuItem(menu, text, listener, false);
	}

	/**
	 * Create a JMenuItem and add it to a JMenu
	 * @param menu : the JMenu to which to add the created JMenuItem
	 * @param text : the text of the JMenuItem
	 * @param listener : the ActionListener of the JMenuItem
	 * @return : the created JMenuItem
	 */
	private JCheckBoxMenuItem addCheckboxMenuItem(JMenu menu, String text,
			ActionListener listener) {
		return (JCheckBoxMenuItem) addMenuItem(menu, text, listener, true);
	}

	/** Generate non-standard mouse cursors */
	private void generateCustomCursors() {
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		nodeToAnimateCursor = toolkit.createCustomCursor(toolkit
				.getImage(getClass().getResource(
						"/resources/images/NodeToAnimate.gif")), new Point(15,
				15), "NodeToAnimate");
	}

	/**
	 * Return an ActionListener that shows a message to inform the user to
	 * select a node on which an animation will take place.
	 * 
	 * @param title1
	 *            : the dialog title
	 * @param message1
	 *            : the dialog message
	 * @param tool
	 *            : the Tool to use
	 * @return : the constructed ActionListener
	 */
	private ActionListener showSelectNodeForAnimationMessage(
			final String title1, final String message1,
			final SelectNodeToAnimateTool tool) {
		return new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				if (isTreeConsistentForAnimation()) {

					JOptionPane.showMessageDialog(MainFrame.this, message1,
							title1, JOptionPane.INFORMATION_MESSAGE);

					selectedTool = tool;
					canvas.setCursor(nodeToAnimateCursor);
				}

			}

		};
	}

	/**
	 * Prepare the application for an animation. Save temporary file in case the
	 * user opts to cancel during the animation.
	 * 
	 * @param animation
	 *            : the AnimationSequence to animate/start.
	 */
	void startAnimation(AnimationSequence animation) {
		if (currentAnimation == null && animation != null) {
			canvas.getFileHandler().saveTemporary();
			layoutModeState = layoutModeMenuItem.isSelected();

			layoutModeMenuItem.setSelected(true);
			canvas.updateTree();
			currentAnimation = animation;
			setAnimationButtonsEnabled(true);
		}
	}

	/**
	 * Enables or disables the animation buttons.
	 * 
	 * @param enabled
	 *            : whether to enable the animation buttons or not
	 */
	private void setAnimationButtonsEnabled(boolean enabled) {
		if (!enabled) {
			playPauseButton.setIcon(playIcon);
			playPauseButton.setToolTipText(PLAY_TOOLTIP);
		}
		for (JButton b : new JButton[] { rewindButton, playPauseButton,
				nextStepButton, fastForwardButton }) {
			b.setEnabled(enabled);
		}
	}

	/**
	 * Creates a new JButton applicable to the toolbar with a given icon.
	 * 
	 * @param imagePath
	 *            : path to the icon
	 */
	private JButton newToolbarIconButton(String imagePath) {

		ImageIcon icon = new ImageIcon(getClass().getResource(imagePath));

		// Resize the icon
		icon = new ImageIcon(icon.getImage().getScaledInstance(BUTTON_SIZE,
				BUTTON_SIZE, Image.SCALE_SMOOTH));

		JButton newButton = new JButton(icon);
		toolbar.add(newButton);

		return newButton;
	}

	/** Refresh the toolbar. Highlight the selected tool and update the cursor. */
	public void updateToolbar() {
		for (Component component : toolbar.getComponents()) {
			component.setBackground(TOOLBAR_COLOUR);
		}
		JButton button = (JButton) toolbar.getComponents()[selectedTool
				.toolbarIndex()];
		button.setBackground(SELECTED_TOOL_COLOR);
		button.requestFocus(); // not really necessary. Aesthetics.

		canvas.setCursor(selectedTool.cursor());
	}

	/**
	 * Show an error message.
	 * 
	 * @param message
	 *            : the message to display.
	 * @param title
	 *            : the title of the error dialog box.
	 */
	public void showErrorMessage(String message, String title) {
		JOptionPane.showMessageDialog(this, message, title,
				JOptionPane.ERROR_MESSAGE);
	}

	/**
	 * Close the application. Ask the user to save if the tree has been
	 * modified.
	 */
	public void closeApplication() {

		int response = JOptionPane
				.showConfirmDialog(
						this,
						"Would you like to save your current tree before closing the application?",
						"Save before closing?",
						JOptionPane.YES_NO_CANCEL_OPTION,
						JOptionPane.QUESTION_MESSAGE);
		if (response == JOptionPane.YES_OPTION) {
			canvas.getFileHandler().saveTree(true);
		} else if (response == JOptionPane.CANCEL_OPTION) {
			return;
		}

		System.exit(0);
	}

	/**
	 * 
	 * @return : whether or not Layout mode is enabled.
	 */
	public boolean isLayoutMode() {
		return layoutModeMenuItem.isSelected();
	}

	/** Enables layout mode if it was disabled, otherwise disables it. */
	public void toggleLayoutMode() {
		layoutModeMenuItem.doClick();
	}

	/**
	 * 
	 * @return : whether or not Sorted mode is enabled.
	 */
	public boolean isSortedMode() {
		return sortedModeMenuItem.isSelected();
	}

	/**
	 * 
	 * @return : whether or not Highlight unbalanced nodes mode is enabled.
	 */
	public boolean isHighlightingUnbalancedNodes() {
		return highlightUnbalancedModeMenuItem.isSelected();
	}

	/**
	 * 
	 * @return : whether or not Show balance factors mode is enabled.
	 */
	public boolean showBalanceFactors() {
		return showBalanceFactorsMenuItem.isSelected();
	}

	/**
	 * 
	 * @return : a reference to the Canvas of the MainFrame.
	 */
	public Canvas getCanvas() {
		return canvas;
	}

	/**
	 * Performs 'cleanup' following the completion or termination of an
	 * animation.
	 * 
	 * @param canceled
	 *            : whether the animation was cancelled (true) or completed
	 *            naturally (false).
	 */
	public void animationComplete(boolean canceled) {
		setAnimationButtonsEnabled(false);
		currentAnimation = null;
		playingAnimation = false;
		if (!canceled) {
			canvas.getFileHandler().deleteTemporaryAnimationFile();
		}
	}

	/**
	 * Checks whether the tree is connected, that all Nodes have contents and
	 * that the tree is sorted. These three conditions are required for a tree
	 * to be consistent.
	 * 
	 * @return : whether the tree is consistent for animation.
	 */
	private boolean isTreeConsistentForAnimation() {
		String title = "Error in tree";
		if (!canvas.isTreeConnected()) {
			showErrorMessage(
					"The tree must be connected to perform an animation", title);
			return false;
		}
		if (!canvas.allNodesHaveContents()) {
			showErrorMessage(
					"All nodes must have contents to perform an animation",
					title);
			return false;
		}
		if (!canvas.isTreeSorted()) {
			showErrorMessage(
					"The tree is not correctly sorted: the binary search tree property is not satisfied.\n"
							+ "Use sorted mode to see which nodes are out of place.",
					title);
			return false;
		}
		return true;
	}

	/**
	 * 
	 * @return : the number of nodes to create based on the JSpinner value.
	 */
	public int getNumberOfNodesToCreate() {
		return (int) numNodesToCreate.getValue();
	}

	/**
	 * Sets the tool selected by the MainFrame.
	 * 
	 * @param tool
	 *            : the Tool to set as selected.
	 */
	void setSelectedTool(Tool tool) {
		selectedTool = tool;
		if (tool != DrawTool.instance) {
			canvas.hideHandles();
		}
		updateToolbar();
	}

}
