// CSC3003S - Capstone Project - CHALK
// Alex Hall & Matthew Welham
// HLLALE006 & WLHMAT002

package gui;

import java.awt.Cursor;
import java.awt.event.MouseEvent;

import tree.Node;

/** Tool that allows the editing of Nodes' contents. */
// Most, if not all, method comments as per Tool.
class EditContentsTool extends Tool {

	private static final Cursor CURSOR = new Cursor(Cursor.TEXT_CURSOR);

	// Singleton pattern
	static EditContentsTool instance;

	EditContentsTool(MainFrame frame) {
		super(frame);
		instance = this;
	}

	@Override
	boolean pressed(Node node, MouseEvent e) {
		return node.editContentsToolPressed(e);
	}

	@Override
	int toolbarIndex() {
		return 2;
	}

	@Override
	Cursor cursor() {
		return CURSOR;
	}

}
