// CSC3003S - Capstone Project - CHALK
// Alex Hall & Matthew Welham
// HLLALE006 & WLHMAT002

package gui;

import java.awt.Cursor;
import java.awt.event.MouseEvent;

import animation.AnimationSequence;

import tree.Node;

/**
 * Abstract class involving Tools for Node selection pertaining to certain
 * animations.
 */
// Most, if not all, method comments as per Tool.
abstract class SelectNodeToAnimateTool extends Tool {

	SelectNodeToAnimateTool(MainFrame frame) {
		super(frame);
	}

	@Override
	boolean pressed(Node node, MouseEvent e) {
		return node.isClicked(e);
	}

	@Override
	void mouseReleased(MouseEvent e) {
		if (activeNode != null) {
			if (activeNode.isClicked(e)) {
				// start the animation if the activeNode is clicked
				frame.startAnimation(animation());
				
				frame.setSelectedTool(DrawTool.instance);
				repaint();
			}
		}
	}

	/**
	 * Method to be inherited.
	 * Returns an AnimationSequence corresponding to the animation to be
	 * performed using the selected Node.
	 */
	abstract AnimationSequence animation();

	@Override
	int toolbarIndex() {
		return -1;
	}

	@Override
	Cursor cursor() {
		return null;
	}

}
