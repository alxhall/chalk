// CSC3003S - Capstone Project - CHALK
// Alex Hall & Matthew Welham
// HLLALE006 & WLHMAT002

package gui;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import tree.Node;

/**
 * Class to be given to a Canvas to handle the saving and loading of the listed
 * tree. Also allows for the exporting of a .png image of the Canvas. Makes use
 * of the fact that Node implements Serializable.
 */
public class FileHandler {
	private static final String FILE_EXTENSION = ".glt";
	private static final String EXPORT_IMAGE_FORMAT = "png";
	public static final String TEMP_ANIMATION_FILE_NAME = "preAnimation.glt";

	/** If this is defined, Save operations will write to this file. */
	private String lastFileUsed = "";

	/**
	 * Save As and Load prompts will default to this directory if it is defined.
	 */
	private String lastDirectoryUsed = ".";

	private Canvas canvas;

	public FileHandler(Canvas canvasReference) {
		canvas = canvasReference;
	}

	/**
	 * Loads a tree from a .glt file chosen by the user. Displays an error
	 * message if an incorrect file is chosen. Asks the user to save the current
	 * tree before loading if the tree has been modified since its last save.
	 */
	public void loadTree() {
		if (askToSave(
				"Would you like to save your current tree before loading one?",
				"Save before loading?")) {

			JFileChooser fileToLoad = new JFileChooser();
			fileToLoad.setCurrentDirectory(new File(lastDirectoryUsed));
			int response = fileToLoad.showOpenDialog(canvas.getGUI());
			if (response == JFileChooser.APPROVE_OPTION) {

				if (!hasFileExtension(fileToLoad.getSelectedFile().toString(),
						FILE_EXTENSION)) {
					canvas.getGUI().showErrorMessage(
							"Must load a file with extension \""
									+ FILE_EXTENSION + "\"",
							"Incorrect file extension");
					return;
				}

				// load the Nodes' details from file
				try {
					FileInputStream fileInputStream = new FileInputStream(
							fileToLoad.getSelectedFile());

					ObjectInputStream objectInputStream = new ObjectInputStream(
							fileInputStream);

					canvas.getNodes().clear();

					Object obj;

					// Note: NOT objectInputStream, will always return 0
					while (fileInputStream.available() > 0) {
						obj = objectInputStream.readObject();
						if (obj instanceof Node) { // should hold for everything
							Node node = (Node) obj;
							node.setCanvas(canvas);
							node.createHandles();
							if (node.getIsSerializedRoot()) {
								canvas.setRoot(node);
							}
							canvas.getNodes().add(node);
						}
					}

					objectInputStream.close();

					canvas.treeChanged = false;

					updateLastFileUsed(fileToLoad.getSelectedFile().toString());

					canvas.repaint();

				} catch (Exception e) {
					e.printStackTrace();
					canvas.getGUI()
							.showErrorMessage(
									"Look out! A Lumberjack!!! The tree could not be loaded.",
									"Error loading file");
				}
			}
		}
	}

	/**
	 * Saves the current tree of the Canvas to file. Can rewrite the file in
	 * use, if such a file exists and if that is the intention. Otherwise, the
	 * user will be prompted to enter a new file to which to save (or to select
	 * an existing one).
	 * 
	 * @param overwrite
	 *            : whether or not the last file used should be overwritten.
	 */
	public void saveTree(boolean overwrite) {
		String fileName = lastFileUsed;
		int response = JFileChooser.APPROVE_OPTION;

		if (overwrite && (!lastFileUsed.equals(""))) {

		} else {
			JFileChooser fileToSave = new JFileChooser();
			fileToSave.setCurrentDirectory(new File(lastDirectoryUsed));
			response = fileToSave.showSaveDialog(canvas.getGUI());

			if (response == JFileChooser.APPROVE_OPTION) {
				fileName = fileToSave.getSelectedFile().toString();
				if (!hasFileExtension(fileName, FILE_EXTENSION)) {
					fileName += FILE_EXTENSION;
				}
			}
		}
		if (response == JFileChooser.APPROVE_OPTION) {

			// save the tree to file
			try {
				FileOutputStream fileOutputStream = new FileOutputStream(
						fileName);
				ObjectOutputStream objectOutputStream = new ObjectOutputStream(
						fileOutputStream);

				for (Node node : canvas.getNodes()) {
					node.setIsSerializedRoot(node == canvas.getRoot());
					objectOutputStream.writeObject(node);
				}

				objectOutputStream.close();

				canvas.treeChanged = false;

				updateLastFileUsed(fileName);

			} catch (Exception e) {
				e.printStackTrace();
				canvas.getGUI().showErrorMessage(
						"Oh dear, a FIRE!!! The tree could not be saved.",
						"Error saving file");
			}
		}
	}

	/** Export an image of the canvas to a .png file chosen by the user. */
	public void exportImage() {

		JFileChooser fileToExport = new JFileChooser();
		fileToExport.setCurrentDirectory(new File(lastDirectoryUsed));

		if (fileToExport.showSaveDialog(canvas.getGUI()) == JFileChooser.APPROVE_OPTION) {

			String fileName = fileToExport.getSelectedFile().toString();
			if (!hasFileExtension(fileName, "." + EXPORT_IMAGE_FORMAT)) {
				fileName += "." + EXPORT_IMAGE_FORMAT;
			}

			File file = new File(fileName);
			if (!file.exists()) {
				try {
					file.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
					canvas.getGUI()
							.showErrorMessage(
									"The file to which to export could not be created.",
									"Error exporting canvas");
				}
			}

			// sleep for a while to allow for dialog to disappear
			try {
				Thread.sleep(200);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}

			try {
				BufferedImage image = new Robot()
						.createScreenCapture(new Rectangle(canvas
								.getLocationOnScreen().x, canvas
								.getLocationOnScreen().y, canvas.getWidth(),
								canvas.getHeight()));
				ImageIO.write(image, EXPORT_IMAGE_FORMAT, file);

			} catch (AWTException e) {
				e.printStackTrace();
				canvas.getGUI().showErrorMessage(
						"Image of canvas could not be generated.",
						"Error creating image");
			} catch (IOException e) {
				e.printStackTrace();
				canvas.getGUI().showErrorMessage(
						"Image of canvas could not be written to file.",
						"Error writing image to file");
			}
		}

	}

	/**
	 * Record the path to the last file used and update the title of the
	 * application to reflect that this is the current working file.
	 * 
	 * @param latestFileUsed
	 *            : the path to the last file used.
	 */
	public void updateLastFileUsed(String latestFileUsed) {
		lastFileUsed = latestFileUsed;
		lastDirectoryUsed = (new File(lastFileUsed)).getParent();
		canvas.getGUI().setTitle(
				MainFrame.DEFAULT_TITLE + "   " + latestFileUsed);
	}

	/**
	 * Prompt the user to save any and all changes to a modified tree.
	 * 
	 * @param message
	 *            : the message to display in the dialog
	 * @param title
	 *            : the title of the dialog
	 * @return : whether or not the user wishes to save the changes
	 */
	public boolean askToSave(String message, String title) {
		if (!canvas.treeChanged) {
			return true;
		}
		int response = JOptionPane.showConfirmDialog(canvas.getGUI(), message,
				title, JOptionPane.YES_NO_CANCEL_OPTION);
		if (response == JOptionPane.YES_OPTION) {
			saveTree(true);
			return true;
		} else if (response == JOptionPane.CANCEL_OPTION) {
			return false;
		} else { // NO clicked
			return true;
		}
	}

	/**
	 * 
	 * @param fileName
	 *            : the name of the file to check
	 * @param extension
	 *            : the extension for which to check
	 * @return : whether or not the given filename matches the given extension
	 */
	public boolean hasFileExtension(String fileName, String extension) {
		return fileName.substring(fileName.length() - extension.length(),
				fileName.length()).equals(extension);
	}

	/**
	 * Load a tree from the temporary file. This file was generated prior to an
	 * animation, and this method is being called because the animation was
	 * cancelled and the user wishes to return to the pre-animation state.
	 */
	public void loadTemporary() {
		File temp = new File(TEMP_ANIMATION_FILE_NAME);

		// load the tree
		try {
			if (!temp.exists()) {
				canvas.getGUI().showErrorMessage(
						"Could not find pre-animation state file!",
						"Error loading temporary file");
				return;
			}

			FileInputStream fileInputStream = new FileInputStream(temp);

			ObjectInputStream objectInputStream = new ObjectInputStream(
					fileInputStream);

			canvas.getNodes().clear();

			Object obj;
			while (fileInputStream.available() > 0) { // Note: NOT
														// objectInputStream,
														// will always
														// return 0
				obj = objectInputStream.readObject();
				if (obj instanceof Node) { // should hold for everything
					Node node = (Node) obj;
					node.setCanvas(canvas);
					node.createHandles();
					if (node.getIsSerializedRoot()) {
						canvas.setRoot(node);
					}
					canvas.getNodes().add(node);
				}
			}

			objectInputStream.close();

			canvas.treeChanged = false;

			canvas.repaint();

			deleteTemporaryAnimationFile();

		} catch (Exception e) {
			e.printStackTrace();
			canvas.getGUI().showErrorMessage("Could not reverse animation.",
					"Error loading pre-animation file");
		}
	}

	/** Delete the temporary animation file. */
	public void deleteTemporaryAnimationFile() {
		File temp = new File(TEMP_ANIMATION_FILE_NAME);

		// relies somewhat on && not causing a delete if the file doesn't exist
		if (temp.exists() && !temp.delete()) {
			canvas.getGUI().showErrorMessage(
					"Could not delete pre-animation state file.",
					"Error deleting temporary file");
		}
	}

	/**
	 * Save a tree to the temporary file. This file is generated prior to an
	 * animation, so that if the animation is cancelled then the tree can be
	 * loaded back to its pre-animation state.
	 */
	public void saveTemporary() {
		File temp = new File(TEMP_ANIMATION_FILE_NAME);
		
		// load the tree
		try {
			FileOutputStream fileOutputStream = new FileOutputStream(temp);
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(
					fileOutputStream);

			for (Node node : canvas.getNodes()) {
				node.setIsSerializedRoot(node == canvas.getRoot());
				objectOutputStream.writeObject(node);
			}

			objectOutputStream.close();

			canvas.treeChanged = false;

		} catch (Exception e) {
			e.printStackTrace();
			canvas.getGUI().showErrorMessage(
					"Could not save pre-animation state.",
					"Error saving pre-animation file");
		}
	}

}
