// CSC3003S - Capstone Project - CHALK
// Alex Hall & Matthew Welham
// HLLALE006 & WLHMAT002

package animation;

/**
 * A class to deal with the mathematics of calculating intermediate values in an
 * animation for a single variable. Several actions make a step. The step tells
 * the action to calculate the new value each turn and pass it into the abstract
 * updateWithAnimationValue method.
 * 
 * @author alexhall
 * 
 */
public abstract class AnimationAction {

	private static final int FAST_FORWARD_DURATION = 100;

	private float duration;

	private final float startValue;

	private final float endValue;

	AnimationStep step;

	private final Interpolator interpolator;

	private boolean completed = false;

	public AnimationAction(int duration, float startValue, float endValue,
			Interpolator interpolator) {
		if (duration < 0) {
			throw new IllegalArgumentException(
					"Cannot set a negative duration.");
		}
		if (interpolator == null) {
			interpolator = Interpolator.LINEAR;
		}
		this.duration = Math.max(1, duration);
		this.startValue = startValue;
		this.endValue = endValue;
		this.interpolator = interpolator;
	}

	/**
	 * Update based on the current time (the parameter), the duration, and the
	 * start time
	 */
	void update(long time) {
		updateWithElapsedFraction((time - step.startTime) / duration);
	}

	/**
	 * Update based on how far (as a fraction between 0 and 1) the action has
	 * proceeded out of its duration. Use >=1 to finish.
	 */
	void updateWithElapsedFraction(float elapsedFraction) {
		if (!completed) {
			if (elapsedFraction >= 1) {
				elapsedFraction = 1;
				step.numActionsCompleted++;
				completed = true;
			}

			updateAnimationValue(startValue + (endValue - startValue)
					* interpolator.interpolatedFraction(elapsedFraction));
		}
	}

	/**
	 * Override this method to define what to do with the calculated
	 * intermediate value (usually set some internal field to that value).
	 */
	public abstract void updateAnimationValue(float value);

	/** Shorten the action so that it finishes quickly. */
	void fastForward() {
		duration = Math.min(duration, FAST_FORWARD_DURATION);
	}

}
