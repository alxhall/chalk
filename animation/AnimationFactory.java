// CSC3003S - Capstone Project - CHALK
// Alex Hall & Matthew Welham
// HLLALE006 & WLHMAT002

package animation;

import java.awt.Color;

import javax.swing.JOptionPane;

import gui.Canvas;
import gui.MainFrame;
import tree.Node;

/**
 * A class for making animation sequences for tree operations. Typically the
 * results of public methods go into MainFrame.startAnimation.
 */
public class AnimationFactory {

	private static final int FIND_PLAY_DELAY = 800;
	private static final Color HIGHLIGHT_FIND_COLOR = Color.green;
	private static final Color HIGHLIGHT_NOT_FOUND_COLOR = Color.red;
	private MainFrame frame;

	public AnimationFactory(MainFrame frame) {
		this.frame = frame;
	}

	public AnimationSequence findAnimation(int toFind) {
		AnimationSequence sequence = new AnimationSequence(frame);
		Canvas canvas = frame.getCanvas();
		Node previous = null, next = canvas.getRoot();
		AnimationStep step;
		boolean notPresent = false;
		while (true) {
			step = new AnimationStep(canvas).setPlayDelay(FIND_PLAY_DELAY);
			if (previous != null) {
				previous.animateSetColor(HIGHLIGHT_FIND_COLOR,
						previous.getUsualColor(), step);
			}
			if (next == null) {
				sequence.addSteps(step);
				break;
			} else {
				next.animateSetColor(HIGHLIGHT_FIND_COLOR, step);
			}
			previous = next;
			if (next != null && next.getContents() == toFind) {
				next = null;
			} else {
				if (next.getLeft() != null && next.getContents() > toFind) {
					next = next.getLeft();
				} else if (next.getRight() != null
						&& next.getContents() < toFind) {
					next = next.getRight();
				} else {
					notPresent = true;
				}
			}
			sequence.addSteps(step);
			if (notPresent) {
				step = new AnimationStep(canvas);
				next.animateSetColor(HIGHLIGHT_NOT_FOUND_COLOR, step);
				sequence.addSteps(step);
				step = new AnimationStep(canvas);
				previous.animateSetColor(HIGHLIGHT_NOT_FOUND_COLOR,
						previous.getUsualColor(), step);
				sequence.addSteps(step);
				break;
			}
		}
		return sequence;
	}

	public AnimationSequence bstInsertAnimation(final int toInsert) {
		final AnimationSequence sequence = new AnimationSequence(frame);
		final Canvas canvas = frame.getCanvas();
		Node previous = null, next = canvas.getRoot();
		AnimationStep step;
		boolean notPresent = false;
		while (true) {
			step = new AnimationStep(canvas).setPlayDelay(FIND_PLAY_DELAY);
			if (previous != null) {
				previous.animateSetColor(HIGHLIGHT_FIND_COLOR,
						previous.getUsualColor(), step);
			}
			next.animateSetColor(HIGHLIGHT_FIND_COLOR, step);

			if (next.getContents() == toInsert) {
				step = new AnimationStep(canvas);
				previous.animateSetColor(HIGHLIGHT_FIND_COLOR,
						previous.getUsualColor(), step);
				next.animateSetColor(HIGHLIGHT_NOT_FOUND_COLOR, step);
				sequence.addSteps(step);
				step = new AnimationStep(canvas);
				next.animateSetColor(HIGHLIGHT_NOT_FOUND_COLOR,
						next.getUsualColor(), step);
				sequence.addSteps(step);
				break;
			} else {
				previous = next;
				if (next.getLeft() != null && next.getContents() > toInsert) {
					next = next.getLeft();
				} else if (next.getRight() != null
						&& next.getContents() < toInsert) {
					next = next.getRight();
				} else {
					notPresent = true;
				}
			}

			sequence.addSteps(step);
			if (notPresent) {
				step = new AnimationStep(canvas);

				// Reset current highlighted node color
				next.animateSetColor(HIGHLIGHT_FIND_COLOR,
						next.getUsualColor(), step);

				// Add new node
				final Node nextFinal = next;
				final Node newNode = new Node(nextFinal.getX(),
						nextFinal.getY(), canvas, toInsert);
				step.addActions(new InstantAnimationAction() {

					@Override
					public void updateAnimationValue(float value) {
						if (nextFinal.getContents() > toInsert) {
							nextFinal.setLeft(newNode);
						} else {
							nextFinal.setRight(newNode);
						}
						newNode.move(nextFinal.getX(), nextFinal.getY());
						canvas.getNodes().add(newNode);
						canvas.updateTree();
					}
				});

				// Highlight new node
				newNode.animateSetColor(HIGHLIGHT_FIND_COLOR, step);

				// Resetting the color of the inserted node
				AnimationStep lastStep = new AnimationStep(canvas)
						.setPlayDelay(FIND_PLAY_DELAY);
				newNode.animateSetColor(HIGHLIGHT_FIND_COLOR,
						newNode.getUsualColor(), lastStep);

				sequence.addSteps(step, lastStep);
				break;
			}
		}
		return sequence;
	}

	/**
	 * Returns a sequence showing the deletion of the given node without messing
	 * up the binary search tree.
	 */
	public AnimationSequence bstDeleteAnimation(final Node toDelete) {

		final AnimationSequence sequence = new AnimationSequence(frame);
		return bstDeleteAddToSequence(toDelete, sequence, true) ? sequence
				: null;
	}

	/**
	 * Adds to the sequence any steps necessary to delete the node. Returns
	 * whether or not the sequence was modified.
	 * 
	 * @param showMessages
	 *            If a message should be shown if a simple deletion (e.g. a
	 *            leaf) is performed with no animation.
	 * 
	 */
	private boolean bstDeleteAddToSequence(final Node toDelete,
			final AnimationSequence sequence, boolean showMessages) {
		final Canvas canvas = frame.getCanvas();

		// If the node has no children
		if (toDelete.getLeft() == null && toDelete.getRight() == null) {
			if (showMessages) {
				JOptionPane
						.showMessageDialog(
								frame,
								"When a leaf is removed, it is simply removed. There are no extra steps needed.",
								"No animation needed",
								JOptionPane.INFORMATION_MESSAGE);
			}
			toDelete.delete();
			return false;
		}

		// If the node has two children
		if (toDelete.getLeft() != null && toDelete.getRight() != null) {

			// Find the smallest child in the right subtree
			Node smallestRight = toDelete.getRight();
			while (smallestRight.getLeft() != null) {
				smallestRight = smallestRight.getLeft();
			}

			// Make it final for the anonymous class
			final Node smallestRightFinal = smallestRight;

			// Replace this node's contents with those of smallestRight and then
			// perform a delete animation on smallestRight
			sequence.addSteps(smallestRight.animateMoveContents(toDelete),
					new AnimationStep(canvas, new InstantAnimationAction() {

						@Override
						public void updateAnimationValue(float value) {
							smallestRightFinal.resetMovingContents();
							toDelete.setContents(smallestRightFinal
									.getContents());
							smallestRightFinal.clearContents();
							bstDeleteAddToSequence(smallestRightFinal,
									sequence, false);
							sequence.nextStep();
						}
					}));

		}

		// If it has one child
		else {

			// The root can just be deleted as it normally would
			if (toDelete.isRoot()) {
				toDelete.delete();
				return false;
			}

			// parent != null because this is not the root
			final Node parent = toDelete.getParent();

			// The one child of this node
			final Node newChild = toDelete.getLeft() != null ? toDelete
					.getLeft() : toDelete.getRight();

			// Make the parent bypass this node and point to this node's one
			// child instead (animate the moving edge)
			AnimationStep setNewChildStep = new AnimationStep(canvas);
			parent.animateMoveEdge(toDelete, setNewChildStep, newChild);
			sequence.addSteps(setNewChildStep, new AnimationStep(canvas,
					new InstantAnimationAction() {

						@Override
						public void updateAnimationValue(float value) {
							parent.resetMovingEdges();
							canvas.getNodes().remove(toDelete);
						}
					}), new AnimationStep(canvas, new InstantAnimationAction() {

				@Override
				public void updateAnimationValue(float value) {
					canvas.updateTree();
				}
			}));

		}

		return true;
	}

	private void rotateWithLeftChild(final Node toRotate,
			final AnimationSequence sequence) {
		rotateAnimationAddToSequence(toRotate, sequence, toRotate.getLeft(),
				toRotate.getLeft().getRight(), false);
	}

	private void rotateWithRightChild(final Node toRotate,
			final AnimationSequence sequence) {
		rotateAnimationAddToSequence(toRotate, sequence, toRotate.getRight(),
				toRotate.getRight().getLeft(), true);
	}

	/**
	 * Construct a single rotation and add it to the sequence (a second call may
	 * be used to perform a double rotation in one sequence)
	 * 
	 * @param toRotate
	 *            The node that was clicked and had a poor balanced factor.
	 * @param sequence
	 *            The sequence to add steps to.
	 * @param replacement
	 *            The node that will take the place of toRotate as the new child
	 *            of toRotate's parent and the new root of toRotate's subtree
	 * @param innerChild
	 *            The child of replacement that will become the child of
	 *            toRotate. Currently a grandchild of toRotate. Possibly null.
	 * @param toRotateBecomesLeft
	 *            The side/direction of the rotation
	 */
	private void rotateAnimationAddToSequence(final Node toRotate,
			final AnimationSequence sequence, final Node replacement,
			final Node innerChild, final boolean toRotateBecomesLeft) {
		final Canvas canvas = frame.getCanvas();
		final Node parent = toRotate.getParent();

		// Make toRotate point to innerChild instead of replacement
		AnimationStep step = new AnimationStep(canvas);
		toRotate.animateMoveEdge(replacement, step, innerChild);
		sequence.addSteps(step);
		step = new AnimationStep(canvas, new InstantAnimationAction() {

			@Override
			public void updateAnimationValue(float value) {
				toRotate.resetMovingEdges();
			}
		});
		
		// Make replacement point to toRotate instead of innerChild
		replacement.animateMoveEdge(innerChild, step, toRotate,
				toRotateBecomesLeft);
		sequence.addSteps(step, new AnimationStep(canvas,
				new InstantAnimationAction() {

					@Override
					public void updateAnimationValue(float value) {
						replacement.resetMovingEdges();
						if (toRotate.isRoot()) {
							replacement.setToRoot();
							canvas.updateTree();
						}
						sequence.nextStep();
					}
				}));
		
		// Make replacement the new child of toRotate's parent
		if (parent != null) {
			step = new AnimationStep(canvas);
			parent.animateMoveEdge(toRotate, step, replacement);
			sequence.addSteps(step, new AnimationStep(canvas,
					new InstantAnimationAction() {

						@Override
						public void updateAnimationValue(float value) {
							parent.resetMovingEdges();
							canvas.updateTree();
						}
					}));
		}
	}

	public AnimationSequence rotateAnimation(final Node toRotate) {
		
		// If one of the children is unbalanced, rotate him instead
		for (Node child : toRotate.children()) {
			if (child != null && Math.abs(child.calculateBalanceFactor()) >= 2) {
				return rotateAnimation(child);
			}
		}
		final AnimationSequence sequence = new AnimationSequence(frame);
		
		// The 4 rotation cases
		if (toRotate.calculateBalanceFactor() <= -2) {
			
			// Single
			if (toRotate.getLeft().calculateBalanceFactor() <= 0) {
				rotateWithLeftChild(toRotate, sequence);
			} else {
				
				// Double
				rotateWithRightChild(toRotate.getLeft(), sequence);
				sequence.addSteps(new AnimationStep(frame.getCanvas(),
						new InstantAnimationAction() {

							@Override
							public void updateAnimationValue(float value) {
								rotateWithLeftChild(toRotate, sequence);
								sequence.nextStep();
							}
						}));
			}
			
		} else if (toRotate.calculateBalanceFactor() >= 2) {

			// Single
			if (toRotate.getRight().calculateBalanceFactor() >= 0) {
				rotateWithRightChild(toRotate, sequence);
			} else {
				
				// Double
				rotateWithLeftChild(toRotate.getRight(), sequence);
				sequence.addSteps(new AnimationStep(frame.getCanvas(),
						new InstantAnimationAction() {

							@Override
							public void updateAnimationValue(float value) {
								rotateWithRightChild(toRotate, sequence);
								sequence.nextStep();
							}
						}));
			}
		} else {
			frame.showErrorMessage(
					"Node must have a balance factor whose absolute value is greater than 1.",
					"Node selection error");
			return null;
		}
		return sequence;
	}
}
