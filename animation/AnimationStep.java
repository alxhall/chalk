// CSC3003S - Capstone Project - CHALK
// Alex Hall & Matthew Welham
// HLLALE006 & WLHMAT002

package animation;

import gui.Canvas;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.Timer;

public class AnimationStep {

	private static final float FRAMES_PER_SECOND = 30;
	private static final int DELAY = (int) (1000f / FRAMES_PER_SECOND);

	static final int DEFAULT_PLAY_DELAY = 2000;

	/**
	 * The amount of time between this step and the next when the sequence is
	 * playing, i.e. the time needed to absorb/understand the step (ms).
	 */
	int playDelay = DEFAULT_PLAY_DELAY;

	int numActionsCompleted = 0;

	List<AnimationAction> actions = new ArrayList<>();

	private Timer timer;

	private Canvas canvas;

	AnimationSequence sequence;

	long startTime;

	public AnimationStep(Canvas canvas, AnimationAction... actions) {
		this.canvas = canvas;
		addActions(actions);
	}

	/** Add actions to the step and get this step back */
	public AnimationStep addActions(AnimationAction... actions) {
		for (AnimationAction a : actions) {
			this.actions.add(a);
			a.step = this;
		}
		maybeFastForward();
		return this;
	}

	/** Fast forward all the actions if the sequence is being fast forwarded. */
	void maybeFastForward() {
		if (sequence != null && sequence.fastForwarding) {
			for (AnimationAction action : actions) {
				action.fastForward();
			}
		}
	}

	/** Start playing this step, updating all its actions every frame. */
	public void start() {

		timer = new Timer(DELAY, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				long time = new Date().getTime();
				for (int i = 0; i < actions.size(); i++) {
					actions.get(i).update(time);
				}
				afterUpdate();
			}

		});
		timer.setInitialDelay(0);
		startTime = new Date().getTime();
		timer.start();
	}

	/** Complete all the actions and end the step */
	public void finish() {
		for (AnimationAction action : actions) {
			action.updateWithElapsedFraction(1);
		}
		afterUpdate();
	}

	/** Returns itself */
	public AnimationStep setPlayDelay(int delay) {
		this.playDelay = delay;
		return this;
	}

	/**
	 * To be performed after all actions have updated in one way or another.
	 * Repaints the canvas and potentially ends the step, informing the sequence
	 * if there is one.
	 */
	private void afterUpdate() {
		canvas.repaint();
		if (numActionsCompleted >= actions.size()) {
			timer.stop();
			if (sequence != null) {
				sequence.stepCompleted();
			}
		}
	}

}
