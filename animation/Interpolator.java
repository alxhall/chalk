// CSC3003S - Capstone Project - CHALK
// Alex Hall & Matthew Welham
// HLLALE006 & WLHMAT002

package animation;

/**
 * A mathematical function which converts an elapsed fraction (a number between
 * 0 and 1 saying how much of an animation has happened) into an interpolated
 * fraction, also a number between 0 and 1, which will be used to interpolate
 * between the start and end values of an animation action, and affects the
 * appearance of the variable changing over time.
 */
public interface Interpolator {

	/**
	 * See the Interpolator javadoc. interpolatedFraction(0) must be 0 and
	 * interpolatedFraction(1) must be 1.
	 */
	public float interpolatedFraction(float elapsedFraction);

	/** The simplest kind of animation, suitable for changing colors. */
	public static final Interpolator LINEAR = new Interpolator() {

		@Override
		public float interpolatedFraction(float elapsedFraction) {
			return elapsedFraction;
		}
	};

	/**
	 * Good for a smooth movement, gives the impression of an initial
	 * acceleration followed by deceleration. The name refers to the shape of
	 * the value-time graph.
	 */
	public static final Interpolator S_SHAPED = new Interpolator() {

		@Override
		public float interpolatedFraction(float x) {
			return (float) Math.min(1, x - Math.sin(2 * Math.PI * x)
					/ (2 * Math.PI));
		}
	};

}
