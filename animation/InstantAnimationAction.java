// CSC3003S - Capstone Project - CHALK
// Alex Hall & Matthew Welham
// HLLALE006 & WLHMAT002

package animation;

/**
 * A convenience class for an action that has no inherent duration or values and
 * is just a thing that must be done once.
 */
public abstract class InstantAnimationAction extends AnimationAction {

	public InstantAnimationAction() {
		super(0, 0, 0, null);
	}

	@Override
	void update(long time) {
		updateWithElapsedFraction(1);
	}

}
