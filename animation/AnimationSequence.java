// CSC3003S - Capstone Project - CHALK
// Alex Hall & Matthew Welham
// HLLALE006 & WLHMAT002

package animation;

import gui.MainFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayDeque;
import java.util.Queue;

import javax.swing.Timer;

public class AnimationSequence {

	private static final int PLAY_START_DELAY = 400;

	private Queue<AnimationStep> steps = new ArrayDeque<>();

	private final MainFrame frame;

	private Timer playTimer = new Timer(AnimationStep.DEFAULT_PLAY_DELAY,
			new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					nextStep();
				}
			});

	private boolean playing = false;

	private AnimationStep currentStep;

	/** true = User clicked fast forward, shorten all actions in all steps */
	boolean fastForwarding = false;

	/**
	 * true = Next step was just clicked, and the current step is being finished
	 * off
	 */
	private boolean nextStepping = false;

	/** true = Rewind/cancel has been clicked, time to clean up */
	private boolean canceled = false;

	public AnimationSequence(MainFrame frame, AnimationStep... steps) {
		this.frame = frame;
		addSteps(steps);
	}

	public AnimationSequence addSteps(AnimationStep... steps) {
		for (AnimationStep step : steps) {
			this.steps.add(step);
			step.sequence = this;

			// A step can be added while a sequence is playing, and hence while
			// it is fast-forwarding
			step.maybeFastForward();
		}
		return this;
	}

	public void nextStep() {
		if (!steps.isEmpty()) {
			currentStep = steps.poll();
			currentStep.start();
		}
	}

	/** Start playing the sequence */
	public void play() {
		play(PLAY_START_DELAY);
	}

	/** Play the sequence with a custom time between steps. */
	private void play(int delay) {
		playTimer.setInitialDelay(delay);
		playTimer.setRepeats(false);
		playTimer.start();
		playing = true;
	}

	/** Pause the sequence when playing. Does not affect the current step. */
	public void pause() {
		playTimer.stop();
		playing = false;
	}

	/**
	 * Called by the current step when all its actions are completed. Possibly
	 * completes the sequence. Handles timing for a step being skipped, or the
	 * sequence being played or fast-forwarded.
	 */
	void stepCompleted() {
		if (steps.isEmpty()) {
			frame.animationComplete(canceled);
			playTimer.stop();
		} else if (playing) {
			playTimer.setInitialDelay(fastForwarding || nextStepping
					|| currentStep == null ? 0 : currentStep.playDelay);
			playTimer.start();
		}
		currentStep = null;
		nextStepping = false;
	}

	/** Start a new step, possibly skipping the currently playing one. */
	public void nextStepClicked() {
		if (currentStep != null) {
			nextStepping = true;
			currentStep.finish();
		} else {
			nextStep();
		}
	}

	/**
	 * Fast forward the whole sequence so that each step is completely very
	 * quickly and there is no delay between.
	 */
	public void fastForward() {
		fastForwarding = true;
		
		// Shortening the steps
		for (AnimationStep step : steps) {
			step.maybeFastForward();
		}
		
		// No delay between steps
		if (playing) {
			if (currentStep == null) {
				playTimer.setInitialDelay(0);
				playTimer.restart();
			} else {
				currentStep.finish();
			}
		} else {
			play(0);
		}
	}

	/** Finish the current step and don't play anything more. */
	public void cancel() {
		canceled = true;
		steps.clear();
		if (currentStep != null) {
			currentStep.finish();
		} else {
			stepCompleted();
		}
	}

}
